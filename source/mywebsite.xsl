<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:html='http://www.w3.org/1999/xhtml'
                version="1.0"
exclude-result-prefixes="html"
>
  <!--*
  <xsl:import href="/sgml/docbook/website/xsl/chunk-website.xsl"/>
  *-->
  <xsl:import href="/usr/share/sgml/docbook/xsl-stylesheets-1.79.2/website/chunk-website.xsl"/>

  <xsl:output method="html"/>


  <xsl:template match="/">
    <html>
      <head>
        <title></title>
<link rel="alternate" type="text/html" media="handheld" 
href="http://mowser.com/web/russellbeattie.com" title="Mobile/PDA" />
      </head>
      <body bgcolor="#FFFFFF">
        <xsl:apply-templates/>
      </body>
    </html>
  </xsl:template>

<xsl:param name="chunker.output.encoding">utf-8</xsl:param>

<xsl:template match="summary" mode="head.mode">
  <!--nop-->  
         <!-- This is used on second phase, called from head.xsl -->   
</xsl:template>





<!-- For typekit fonts -->
<!-- Use google fonts -->
<xsl:template name="user.head.content">
  <xsl:param name="node" select="."/>
  <link href='https://fonts.googleapis.com/css?family=Dosis|Overlock&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css' />

<!-- font-family: 'Open Sans', sans-serif; font-family: 'Gentium Basic', serif;-->

<link href='https://fonts.googleapis.com/css?family=Gentium+Basic|Open+Sans' rel='stylesheet' type='text/css' />

</xsl:template>

<!-- see docbook file which creates html element -->
<xsl:variable name="incl.adsense" select="true()"/>
<xsl:variable name="oxygen" select="false()"/>


<xsl:template match="summary" >
  <span style="color:#7C7C7C;"><xsl:apply-templates/></span>
</xsl:template>

</xsl:stylesheet>
