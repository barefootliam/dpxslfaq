<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2001-06-10 09:49:04 dave"   -->
<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="N1000">

<head>
<title>Indexing in XSLT</title>
<summary>Indexing</summary>
  <keywords>index Index indexing</keywords>
      <description>Indexing XML</description>
</head>
<qandaset>

 

 

 

  <qandaentry>
   <question>
    <para>Index into XML files</para>

   </question>
   <answer>
    <para role="author">DaveP</para>
    <para><emphasis>Background.</emphasis></para>
    <para>I kept having requests for a better index for the XSLT faq from users. Perhaps because its grown up over time, or I'm no librarian, but others view of where I should file something rarely agreed with mine. An index seemed an obvious answer.</para>
    <para>I guessed I could use XSLT to do it, and after thinking for a while, decided I wanted a general purpose system, that I could expand as needed. End goal was an index file(s) of terms, the data source to be indexed, and an output of an html file pointing into the resultant html files derived from the data source. That was the requirement</para>
 
    <itemizedlist>
     <title>Issues</title>
     <listitem>
 
      <para>The data source I was indexing was in XML, the links needed to be in HTML, having been created by someone elses stylesheets (Norm Walsh's docbook Stylesheets) - which means I need to modify them as little as possible. </para>
     </listitem>
     <listitem>
      <para>The datasources were in about 200 seperate files (last time I looked), and were in no way static, being updated weekly. So the index build had to be automated.</para>
     </listitem>
  <listitem>
      <para>I knew it would bust XSLT 1.0, because I would have to use node-set to convert to node-sets from result tree fragments, but I guess that will be fixed as soon as 1.1 is released.</para>
     </listitem>
  <listitem>
      <para>I was neither familiar with the document function to search the input, nor the keys function, both of which I knew I would need to avoid duplicates in the output.</para>
     </listitem>
  <listitem>
      <para>If I wanted indexing to the nearest parent element of the node containing the sought term, I would have to modify the docbook stylesheets.</para>
     </listitem>
 <listitem>
      <para>Apart from that it was just difficult</para>
     </listitem>
    </itemizedlist>
    <para>With that in front of me I made a start! The source (term) files was a  simple word list, such as the example below.</para>
    <programlisting format="linespecific">
&lt;?xml version="1.0"?>
&lt;words>
&lt;word>boolean(&lt;/word>
&lt;word>ceil(&lt;/word>
&lt;word>ceiling(&lt;/word>
&lt;word>concat(&lt;/word>
&lt;word>contains(&lt;/word>
&lt;word>count(&lt;/word>
&lt;word>current(&lt;/word>
&lt;word>document(&lt;/word>
&lt;word>element-available(&lt;/word>
&lt;word>false(&lt;/word>
&lt;word>floor(&lt;/word>
&lt;word>format-number(&lt;/word>
&lt;word>function-available(&lt;/word>
&lt;word>generate-id(&lt;/word>
etc. 
</programlisting>
    <para>The terminating left paren is there to seperate a word current from the function current(). I didn't want that to show in the final output, but sub-string() provided a quick answer to that one.</para>
    <para>I knew it would be a two stage affair. I had to walk through each data file comparing certain contents with each word in the word list. This meant a double for-each, to change context each time.</para>
    <para>The intermediate file format is again straightforward. I could have done it... correction, it could have been done, in one stylesheet, but I felt that beyond me, and there would be no way I could maintain it. It looks like this</para>
    <programlisting format="linespecific">
&lt;?xml version="1.0" encoding="utf-8"?>
&lt;faqindex>
	&lt;popular>
		&lt;pair>
			&lt;word>count&lt;/word>
			&lt;file>list.html#para-13&lt;/file>
		&lt;/pair>
		&lt;pair>
			&lt;word>reference&lt;/word>
			&lt;file>list.html#para-9&lt;/file>
		&lt;/pair>
	&lt;/popular>
	&lt;functions/>
	&lt;attributes/>

	&lt;functions/>
	&lt;attributes/>

	&lt;functions>
		&lt;pair>
			&lt;word>document&lt;/word>
			&lt;file>N169.html#para-9&lt;/file>
		&lt;/pair>
	&lt;/functions>
&lt;/faqindex>

</programlisting>
    <para>Within the outer wrapper, there are three elements, matching each of the source term files, each of which takes the same format. These three elements take the form of a wrapper for a word and file pair. The first is the term I wanted indexing, the second the file plus the fragment identifier of the first parent of the data source element containing the term. The location within the html file is identified using a format obtained from the number function, applied to the element, which only has to be unique to the file. Jeni Tennison provided this routine.</para>
    <para>So, thats the index term files, the intermediate file produced, all thats needed now is a couple of stylesheets to search the data source and index term files, and a second one to produce the actual html index. This was made simpler for me by having the output files all in the same directory. I could have produced another docbook compliant file, and may sometime, but for the moment I'll keep it simple.</para>
    <para>Firstly then, the stylesheeet to produce the intermediate file. I'll break it up to explain each section of code. The file is used with the data source files as the input document, creating a temporary output file used by the second stylesheet.</para>
    <programlisting format="linespecific">
  &lt;!DOCTYPE xsl:stylesheet [
  &lt;!ENTITY sp "&lt;xsl:text> &lt;/xsl:text>">
  &lt;!ENTITY dot "&lt;xsl:text>.&lt;/xsl:text>">
  &lt;!ENTITY nl "&amp;#10;">
&lt;!ENTITY  rsqb	"&amp;#x005D;">&lt;!--	# RIGHT SQUARE BRACKET -->
&lt;!ENTITY  lt	"&amp;#x003C;">&lt;!--	# LESS-THAN SIGN -->
&lt;!ENTITY  gt	"&amp;#x003E;">&lt;!--	# GREATER-THAN SIGN -->


]>


 &lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                 version="1.0"

                xmlns:xt="http://www.jclark.com/xt"
                extension-element-prefixes="xt">

   &lt;xsl:strip-space elements="*"/>
&lt;xsl:output  
  method="xml" 
  indent="yes"   />
    </programlisting>
    <para>Standard stylesheet, needing James Clarks xt:node-set extension to produce the node-sets from result tree fragments. These use the xt: prefix.</para>

<programlisting>

  &lt;xsl:template match="/">
    &lt;faqindex>
    &lt;xsl:apply-templates/>
  &lt;/faqindex>
  &lt;/xsl:template>

  &lt;xsl:template match="website">
    &lt;xsl:apply-templates/>
  &lt;/xsl:template>

  &lt;xsl:template match="homepage">
   &lt;xsl:apply-templates/>
  &lt;/xsl:template>
    </programlisting>
    <para> The above templates simply take me down to the element of interest within the data source files which are being indexed. Change these to match the wrapper <emphasis>above</emphasis> the ones you are interested in!</para>
<programlisting>
&lt;!-- Process each webpage child in directory xsl -->
&lt;xsl:template match="webpage[config/@value='xsl']">
  &lt;xsl:variable name="file" select="config[@param='filename']/@value"/>
  &lt;xsl:variable name="answers"
                select="qandaset/qandaentry/answer/screen |
                        qandaset/qandaentry/answer/para" />

    </programlisting>
    <para>The predicate on the template match is simply because I have xml files which I don't want to index. All the ones of interest contain an attribute 'value' to element config which contains the value xsl.</para>
    <para>Two variables are created:</para>
    <para>The <emphasis>answer</emphasis> variable holds all the elements of interest which may contain one of the terms I'm searching for.</para>
    <para>The <emphasis>file</emphasis> variable holds the filename of the html file which will be produced from the xml data file I'm searching. Does that make sense? It happens to be held in the config/@param='filename' path in the source file. Thanks Norm!</para>
<programlisting>
 
  &lt;xsl:choose>

    &lt;xsl:when test="qandaset">
     &lt;!-- Check, for ea word popular file-->
      &lt;popular>
        &lt;xsl:for-each select="document('popular.xml')/words/word">
          &lt;xsl:variable name="word" select="."/>
          &lt;xsl:for-each select="$answers[contains(., $word)]">
            &lt;pair>
              &lt;word>
                &lt;xsl:value-of select="$word"/>
              &lt;/word>
              &lt;file>
                 &lt;xsl:value-of select="$file"/>
                &lt;xsl:text>#&lt;/xsl:text>
                &lt;xsl:apply-templates select="." mode="get-id" />
              &lt;/file>
            &lt;/pair>
          &lt;/xsl:for-each>
        &lt;/xsl:for-each>
      &lt;/popular>

    </programlisting>
    <para>The document function is used to pick up the words I want indexing. Don't, as I did, initially, forget the / before the words element (i.e. try to use words/word). Remember that there is the funny root node to account for!</para>
    <para>The xsl:choose is used to isolate the qandaset children from any others. This is merely a precaution for future expansion. The otherwise clause simply processes children, since any webpage may contain other webpages which may be of interest for indexing. Good is docbook!</para>
    <para>The actual when clause does the work.</para>
    <para>The logic reads something like: for-each word in the term file, search the data source elements for any which contain that word. I could have reduced the file size output by using an xsl:if, but its not particularly large anyway, so I didn't! The substring-before function (in the for-each below) is used to output only the word, not the left parenthesis, into the output file.</para>
    <para>This process is repeated for each of the other term files (words.xml and attributes.xml. Each has a different terminator used for my own purpose. The simplest one is the popular.xml example, which outputs only the word itself.</para>
<programlisting>


      &lt;!-- Check, for ea word FUNCTIONS file-->
      &lt;functions>
        &lt;xsl:for-each select="document('words.xml')/words/word">
          &lt;xsl:variable name="word" select="."/>
          &lt;xsl:for-each select="$answers[contains(., $word)]">
            &lt;pair>
              &lt;word>
                &lt;xsl:value-of select="substring-before($word,'(')"/>
              &lt;/word>
              &lt;file>
                 &lt;xsl:value-of select="$file"/>
                &lt;xsl:text>#&lt;/xsl:text>
                &lt;xsl:apply-templates select="." mode="get-id" />
              &lt;/file>
            &lt;/pair>
          &lt;/xsl:for-each>
        &lt;/xsl:for-each>
      &lt;/functions>


      &lt;!-- Check, for ea word attributes file.-->
      &lt;attributes>
	&lt;xsl:for-each select="document('attributes.xml')/words/word">
	  &lt;xsl:variable name="word" select="."/>
	  &lt;xsl:for-each select="$answers[contains(., $word)]" >
	  &lt;pair>
	    &lt;word>
	      &lt;xsl:value-of select="substring-before($word,'=')"/>
	    &lt;/word>
	    &lt;file>
	      &lt;xsl:value-of select="$file"/>
	      &lt;xsl:text>#&lt;/xsl:text>
	      &lt;xsl:apply-templates select="." mode="get-id" />
	    &lt;/file>
	  &lt;/pair>
	&lt;/xsl:for-each>
	&lt;/xsl:for-each>
      &lt;/attributes>
    &lt;/xsl:when>


    &lt;xsl:otherwise>
      &lt;xsl:if test="webpage">
        &lt;xsl:apply-templates />
      &lt;/xsl:if>
    &lt;/xsl:otherwise>
  &lt;/xsl:choose>
&lt;/xsl:template>

    </programlisting>
    <para>This little template is a jem. Guess who designed it? Thanks Jeni Tennison (again)! Its used twice. Once here to provide (whether needed or not) to provide a target fragment identifier for the href, and within docbook, for the same elements, to provide the id value!</para>
    <para>The actual value generated is the element name (para or screen), concatenated with a hyphen and the value of the xsl:number which is sufficiently unique within the file! Very clever.</para>
    <para>Just in case Norm wants to pick it up, the docbook file modified is firstly verbatim.xsl, which I modified to read</para>

    <programlisting format="linespecific">
&lt;xsl:template match="programlisting|screen|literallayout[@class='monospaced']">
  &lt;pre class="{name(.)}">
    &lt;xsl:attribute name="id">
      &lt;xsl:apply-templates select="." mode="get-id" />
    &lt;/xsl:attribute>&lt;xsl:apply-templates/>&lt;/pre>
&lt;/xsl:template>
</programlisting>
    <para>And block.xsl, modified as shown below.</para>
    <programlisting format="linespecific">
&lt;!-- Modified, DP to add the id value by call to new template -->
&lt;xsl:template match="para">
  &lt;p>
    &lt;xsl:attribute name="id">
      &lt;xsl:apply-templates select="." mode="get-id" />
    &lt;/xsl:attribute>
    &lt;xsl:apply-templates/>
  &lt;/p>
&lt;/xsl:template>
</programlisting>

    <para> The same template is added to Norms docbook ones, to provide the function which similarly calculates the id value for the para and screen. If you want to index into various elements, then a similar call can be added to any list of elements. Easy?</para>

<programlisting>

&lt;xsl:template match="para | screen" mode="get-id">
  &lt;xsl:value-of select="local-name()" />
  &lt;xsl:text>-&lt;/xsl:text>
  &lt;xsl:number level="multiple" />
&lt;/xsl:template> 

  &lt;xsl:template match="*"/>
&lt;/xsl:stylesheet>
</programlisting>
    <para>And thats it. The output file is exemplified above, and is used as an input to the second stylesheet.</para>
    <para>The second stage of the index production has the simple input file and the desired output is a list of links, highlighting the words and linking to the file which contains those words. It could perhaps be improved by providing more information (perhaps the title of the file?), but in order to present well, I've kept it simple.</para>
    <para> The following stylesheet does that, chopping it up into three tables to minimise file size. Very straightforward and just about self documenting. Note the double usage of xt:node-set to sort and re-use.</para>

    <programlisting format="linespecific">
   &lt;!DOCTYPE xsl:stylesheet [
  &lt;!ENTITY sp "&lt;xsl:text> &lt;/xsl:text>">
  &lt;!ENTITY dot "&lt;xsl:text>.&lt;/xsl:text>">
  &lt;!ENTITY nl "&#10;">
&lt;!ENTITY  rsqb	"&#x005D;">&lt;!--	# RIGHT SQUARE BRACKET -->
&lt;!ENTITY  lt	"&#x003C;">&lt;!--	# LESS-THAN SIGN -->
&lt;!ENTITY  gt	"&#x003E;">&lt;!--	# GREATER-THAN SIGN -->


]>


 &lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                 version="1.0"

                xmlns:xt="http://www.jclark.com/xt"
                extension-element-prefixes="xt">

   &lt;xsl:strip-space elements="*"/>
&lt;xsl:output  
  method="html" 
  indent="yes"   />


  &lt;xsl:template match="/">
    &lt;html>
      &lt;head>Index&lt;/title>
      &lt;/head>
      &lt;body>
	&lt;xsl:apply-templates/>
      &lt;/body>
    &lt;/html>
  &lt;/xsl:template>

&lt;xsl:template match="faqindex"> 
     &lt;H2>Index&lt;/H2> 
     &lt;p>Rev 1.2 2 Sept 2000:&lt;/p>




     &lt;!-- Sort the functions --> 
       &lt;xsl:variable name="fns"> 
        &lt;xsl:for-each select="functions[*]"> 
          &lt;xsl:sort data-type="text" select="pair/word"/> 
          &lt;xsl:copy-of select="." /> 
        &lt;/xsl:for-each> 
       &lt;/xsl:variable> 

&lt;!-- now convert that to a node-set and step through it --> 
  &lt;H3>&lt;a name="fns">XSLT functions&lt;/a>&lt;/H3> 
 
    </programlisting>
    <para>I chose four column output which matched my usage. I could have used Mike Browns idea to terminate the last row, but I seldom get around to such niceties! Also of note, I failed to use the Muenchian technique to isolate duplicate files! The three tables are identical in usage, hence I only show one.</para>
    <programlisting>


&lt;!-- 4 column output is about right for this word list. -->
  &lt;xsl:variable name="cols" select="4"/>


&lt;!-- Try and tabulate the functions. -->
  &lt;xsl:variable name="sortedFns">
    &lt;xsl:for-each select="xt:node-set($fns)/functions[*]"> 
      &lt;xsl:variable name="fn" select="pair/file"/> 
      &lt;xsl:if test="not(preceding-sibling::attributes/pair/file= $fn)"> 
	&lt;a href="{pair/file}">* &lt;xsl:value-of select="pair/word"/>&lt;/a>&lt;br /> 
      &lt;/xsl:if> 
    &lt;/xsl:for-each> 
  &lt;/xsl:variable>


  &lt;table border="1">
    &lt;xsl:for-each select="xt:node-set($sortedFns)/a[position() mod $cols = 1]">
      &lt;tr> &lt;xsl:apply-templates
	select=". | following-sibling::a[position() &lt; $cols]" mode="Frow"/> 
      &lt;/tr>
    &lt;/xsl:for-each>
  &lt;/table>




 
   &lt;/xsl:template> &lt;!-- End of main template! -->




&lt;!-- Now the general templates, used all 3 secttions -->



   &lt;xsl:template match="a" mode="row">
     &lt;td>
       &lt;xsl:copy>
	 &lt;xsl:copy-of select="@*"/>
	 &lt;xsl:value-of select="."/>
       &lt;/xsl:copy>&lt;/td>
   &lt;/xsl:template>
 

   &lt;xsl:template match="a" mode="Frow">
     &lt;td>
       &lt;xsl:copy>
	 &lt;xsl:copy-of select="@*"/>
	 &lt;xsl:value-of select="."/>()
       &lt;/xsl:copy>&lt;/td>
   &lt;/xsl:template>
 





  &lt;xsl:template match="*">
    &lt;p style= "color:red">*********&lt;xsl:value-of select="name(..)"/>/&lt;xsl:value-of select="name()"/>*********&lt;/p>
  &lt;/xsl:template>
 
&lt;/xsl:stylesheet>
</programlisting>
    <para>Rather a long faq, but since I struggled with the principles for so long, I thought I could save others having a similar need.</para>
    <para>Hope you like it! DaveP</para>

   </answer>
  </qandaentry>

 <qandaentry>
   <question>
    <para>Building a grouped index in XSL</para>

   </question>
   <answer>
    <para role="author">Michael Kay</para>
    <programlisting format="linespecific">

> I need to build an index in an XSL stylesheet that has a
> variable number of
> entry groupings ( numbers, a-e, f-i, etc..).  

	</programlisting>
<para>Use a Muenchian grouping, as described on www.jenitennison.com, with a
grouping key of</para>

<programlisting>translate(substring(., 1, 1), "abcdefg...ABCDEFG...", 
	  "aaaaafffffiiiii...")
</programlisting>
   </answer>
  </qandaentry>

 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
