<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2001-06-10 12:11:24 dave"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N6258">
<head>
<title>Simplified Syntax</title>
<summary>Simplified Syntax</summary>
    <keywords>xslt simplified</keywords>
      <description>xslt simplified syntax</description>
</head>
<qandaset>
<qandaentry>
    
        <question>
<para>How to use XSLT simplified syntax</para>
</question>
    
        
    
        
      
            
    
        
    
        <answer>
<para role="author">Steve Muench</para>
      
            

    <para>Using the simple form gives up some features to achieve
simplicity of representation for people who want to continue
using their existing HTML editors to edit their "HTML
page with data plugged in" as they learn to exploit the
basics of XSLT.
</para>
    <para>The key limitation
is that you cannot use "top-level" XSLT-namespace
elements like:
</para>
    <programlisting>  xsl:import
  xsl:include
  xsl:strip-space
  xsl:preserve-space
  xsl:output
  xsl:key
  xsl:decimal-format
  xsl:namespace-alias
  xsl:attribute-set
  xsl:param
</programlisting>
    <para>in a "simple form" stylesheet. The feeling in the
XSL Working Group was, if someone began to
"discover" the need for these facilities, then they
were venturing beyond the "simple-case" stage, and
at that point we could assume they were mentally
"ready" to learn about an enclosing
&lt;xsl:stylesheet&gt; element and what &lt;xsl:template&gt;
is used for.
</para>
    <para>=== Some Background ===
</para>
    <para>The "simple form" of an XSLT stylesheet -- known in
the spec as "Literal Result Element as Stylesheet"
http://www.w3.org/TR/xslt#result-element-stylesheet -- was
conceived as a mechanism to allow someone familiar with HTML
to continue to use their familiar HTML editing tools to work
on stylesheets that only need a single, root template. The
feature was designed as a smooth-slope introduction
capability to be able to help people who knew HTML begin to
use XSLT without understanding the Spec cover to cover.
</para>
    <para>One very common case that the "simple form" caters
to, is the one where someone is formatting database query
results into an HTML page. Many Ecommerce apps are doing
this, making use of XML/XSLT, a stylesheet like:
</para>
    <programlisting>&lt;html xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xsl:version="1.0"&gt;
  &lt;body&gt;
    &lt;table&gt;
      &lt;!-- Imagine a "shopping cart" query --&gt;
      &lt;xsl:for-each select="ROWSET/ROW"&gt;
        &lt;tr&gt;
          &lt;td&gt;&lt;xsl:value-of 
	select="ITEMNAME"/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;xsl:value-of 
	select="PRICE"/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;xsl:value-of 
	select="QUANTITY"/&gt;&lt;/td&gt;
          &lt;td&gt;&lt;xsl:value-of 
	select="PRICE*QUANTITY"/&gt;&lt;/td&gt;
        &lt;/tr&gt;
      &lt;/xsl:for-each&gt;
    &lt;/table&gt;
  &lt;/body&gt;
&lt;/html&gt;
</programlisting>
    <para>This stylesheet can still edited using FrontPage,
DreamWeaver, etc. With the simple form, once you teach
HTML-savvy people that they:
</para>
    <simplelist>
     <member>  (1) Add an XSLT namespace declaration and
      xsl:version="1.0" to their &lt;html&gt; element, and</member>
     <member>  (2) Combine &lt;xsl:for-each&gt; for looping and
      &lt;xsl:value-of&gt; to "plug in" dynamic data</member>
     <member>  (3) Use /a/b/c "directory-like" notation to refer
      to elements in the source document (the way
      I explain the basics of XPath to beginners)</member>
</simplelist>
    <para>Then they can begin to get productive without having to
understand the XSLT spec cover to cover.
</para>
    <para>Steve Muench Adds:
</para>
    <para>From the spec, in the "DTD Fragment for XSL Stylesheets" you
can see what xsl-namespace attributes are allows on literal
results elements:
</para>
    <programlisting>&lt;!ENTITY % result-element-atts '
  xsl:extension-element-prefixes CDATA #IMPLIED
  xsl:exclude-result-prefixes CDATA #IMPLIED
  xsl:use-attribute-sets %qnames; #IMPLIED
  xsl:version NMTOKEN #IMPLIED
'&gt;
</programlisting>
    <para>So, using a "simple form" of a stylesheet like...
</para>
    <programlisting>  &lt;html xmlns:xsl="http://www.w3.org/1999/XSL/Transform
        xsl:version="1.0"&gt;
    &lt;body&gt;
       :
    &lt;/body&gt;
  &lt;/html&gt;
</programlisting>
    <para>...in addition to the xsl:version="1.0" which is
mandatory, you can optionally add
xsl:extension-element-prefixes, xsl:exclude-result-prefixes,
and xsl:use-attribute-sets.  So, using a simple form
stylesheet does *NOT* mean you give up extensibility or
ability to control namespaces on the output.
</para>
    <para>The only requirement to use these xsl: attributes is that
the xsl namespace be in scope on the element where you use
them. In my example above, if I'd include them on the
&lt;html&gt; element I'd be fine since that element
declares the "xsl" namespace.
</para>
    <para>Within the "simple" stylesheet you can use any XSLT
"action" which is valid to use inside a template.
Remember that the "simple form" is a shortcut for
defining a stylesheet with a single match="/"
template containing it as the template "body".
</para>

            

    
        </answer>
  
    </qandaentry>
</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
