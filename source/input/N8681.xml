<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-06-26 13:03:07 dpawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N8681">

<head>
<title>Lists</title>
<summary>Lists</summary>
      <keywords>xsl, xsl-fo, lists, list labels</keywords>
      <description>xsl-fo lists, label positioning</description>

</head>
<qandaset>


   <qandaentry>
      <question>
	<para>list item writes over the label?</para>
      </question>
      <answer>
	<para role="author">Eliot Kimber</para>
	<literallayout>

> I am trying to use a list and the list-item is writing over the 
> list-label.  
</literallayout>


<para>You have to set the end-indent of the list item label and the
start-indent of the list item block so that they don't overlap. It
took me forever to finally figure out this aspect of FO .</para>

<para>The trick is that you set the provisional label length and label-to-body gap on the list block and then use the "label-end()" and "body-start()" 
functions to calculate the necessary indents:</para>
	  <programlisting>
  &lt;fo:list-block provisional-label-separation="4mm"
      provisional-distance-between-starts="20mm">
    &lt;fo:list-item>
     &lt;fo:list-item-label end-indent="label-end()">
       ...
     &lt;/fo:list-item-label>
     &lt;fo:list-item-body start-indent="body-start()">
        ...
     &lt;/fo:list-item-body>
   &lt;/fo:list-block>
</programlisting>
<para>
This creates a 20mm label "column" and reserves 4mm gutter out of that 20mm.</para>

      </answer>
    </qandaentry>


<qandaentry>
    
        <question>
<para>Table of contents</para>
</question>
    
        
    
        
      
            
    
        
    
        <answer>
<para role="author">Peter Hilton</para>
      
            <programlisting>
&gt;I am trying to set up a table of contents with XSL, 
&gt;and there are two things
&gt;I haven't figured out how to do.  These are:
&gt;1. A right-justified page number for each entry.

You can treat each list entry as a list entry, with the
table of contents entry as the list item label, and the page
number as the list item body:


&lt;fo:list-item-label&gt;
  &lt;fo:block text-align="start"&gt;&lt;xsl:value-of
  select="heading"/&gt;&lt;/fo:block&gt;
&lt;/fo:list-item-label&gt;
&lt;fo:list-item-body&gt;
  &lt;fo:block text-align="end"&gt;
	&lt;fo:page-number-citation/&gt;&lt;/fo:block&gt;
&lt;/fo:list-item-body&gt;

Unfortunately, FOP doesn't seem to support fo:page-number-citation :-(


            </programlisting>
    
        </answer>
  
    </qandaentry>
<qandaentry>
    <question>
<para>How to present a list in XSL</para>
</question>
    
    
      
    
    <answer>
<para role="author">Nikolai Grigoriev</para>
      <programlisting>


&gt;&lt;fo:block &gt;
&gt; &lt;fo:list-block &gt;
&gt;   &lt;xsl:for-each select="path wanted"&gt;
&gt;      &lt;fo:list-item&gt; 
&gt;     &lt;fo:list-item-label&gt;
&gt;        &lt;fo:block&gt;&amp;bull;&lt;/fo:block&gt;
&gt;     &lt;/fo:list-item-label&gt;
&gt;   &lt;fo:list-item-body&gt;
&gt;     &lt;fo:block&gt;&lt;xsl:value-of select="."/&gt;&lt;/fo:block&gt;
&gt;   &lt;/fo:list-item-body&gt;
&gt;        &lt;/fo:list-item&gt;
&gt;   &lt;/xsl:for-each&gt;
&gt;&lt;/fo:list-block&gt;
&gt;&lt;fo:block&gt;
&gt;
&gt;Does that make sense please?

Yes, it does - in FOP and in our FO2PDF; but not in the XSL
WD.  (2000) Both processors expect you to express lists this
way; you can control bullet position by
provisional-distance-between-starts and
provisional-label-separation attributes in
&lt;fo:list-block&gt;.

However, if our processors were conformant, your list would
be wrong.  The problem is that:

1) start-indent and end-indent for both fo:list-item-label
    and fo:list-item-body are calculated relative to the
    surrounding reference-area (i.e. page in most cases);

2) start-indent and end-indent are inheritable.

Therefore, fo:list-item-label in your list should overlap
with fo:list-item-body, and at this point, the formatter is
obliged to issue an error message. Instead, you should
write:

&lt;fo:list-item&gt; 
   &lt;fo:list-item-label end-indent="label-end()"&gt;
      &lt;fo:block&gt;&amp;bull;&lt;/fo:block&gt;
   &lt;/fo:list-item-label&gt;
   &lt;fo:list-item-body start-indent="body-start()"&gt;
      &lt;fo:block&gt;&lt;xsl:value-of select="."/&gt;&lt;/fo:block&gt;
   &lt;/fo:list-item-body&gt;
 &lt;/fo:list-item&gt;

(This is a thing I dislike most in the whole WD. I argued
against it for a long time, but in vain. This way of
building lists, already present in WD 1999-04-21, has been
reconfirmed and backed up with examples in the recent
draft.)

Conclusion. You face a dilemma:
- - if you want to render something with the present-day tools -
  you are on the right way;
- - if you want to compose a conformant FO - you are wrong.

One more comment: &lt;fo:list-block&gt; is a block-level object
itself.  You need not wrap it into a &lt;fo:block&gt;: every
block-level property can be specified directly on the
&lt;fo:list-block&gt;, and every place where &lt;fo:block&gt; can
go will accept &lt;fo:list-block&gt; as well.


</programlisting>
    </answer>
  </qandaentry>
 

 <qandaentry>
   <question>
    <para>Nested lists in xsl-fo</para>

   </question>
   <answer>
    <para role="author">Toshihiko Makita</para>
     <literallayout format="linespecific" class="normal">
 > Finding problems with nested lists.
 > the problem is that the nested list starts on the start-edge,
 > rather than being indented from the parent list.
 </literallayout>
<para>
	  This is not bug. According to the XSL CR, <sgmltag>fo:list-item-body</sgmltag> 
 generates no
 reference areas. So the nested list's reference area is the 
 same as parent
	  <sgmltag>fo:list-item-body</sgmltag>'s reference area. If you want to indent 
 nested lists from
	  parent <sgmltag>fo:list-item-body</sgmltag>'s indent position, please specify 
 inherited values
 by using from-parent() function.</para>
 
	<literallayout> [before]
 &amp;lt;fo:list-block start-indent="0.25in"
 provisional-distance-between-starts="10mm"
 provisional-label-separation="5mm">
 
 ==>Explicitly specifying start-indent="0.25in" cancels 
 inherited value. In
 this context, inherited value is parent fo:list-item-body's 
 start-indent
 (start-indent="body-start()")</literallayout>
 
	<literallayout> [after]
 &amp;lt;fo:list-block start-indent="from-parent(start-indent) + 0.25in"
 provisional-distance-between-starts="10mm"
 provisional-label-separation="5mm">
 </literallayout>



   </answer>
  </qandaentry>
 
    <qandaentry>
      <question>
	<para>Body-start calcuation for lists</para>
      </question>
      <answer>
	<para role="author">Tony Graham</para>
	<literallayout>


> > The spec says: "When this function is used outside of a list, it still
> > returns a calculated value as specified."
> > (http://www.w3.org/TR/xsl/slice5.html#section-N8624-Property-Value-Functions
> > ) 
> What is the 'calculated value as specified' outside of a list?
</literallayout>

	<literallayout>body-start() = the value of the start-indent +
               start-intrusion-adjustment + the value of the
               provisional-distance-between-starts of the closest
               ancestor fo:list-block
</literallayout>
<para>David Tolpin adds</para>

<para>The value of the start-indent of what? In the presence of the list-block, it is the value
of start-indent of the list-block. In the absence of start-indent, it is the value of
nothing.</para>

      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Lists, and nested lists</para>
      </question>
      <answer>
	<para role="author">Mike Ferrando</para>


<para> wrote it for EAD 2002 tag set not HTML. But the
elements (LIST and ITEM) and attributes are almost exactly the same.</para>
<para>
Here it is.</para>
<para>
My goals:</para>
<para>
 To indent only on the presence of a nested LIST element </para>
<para> 
 The syntax should be exactly the same (LIST or ITEM) to
increase the variable when there is a nested list (recursive) 
 LIST/LIST</para>
 
<para>Does not receive bullets (or whatever) based on the variable value. This is
a local preference. I have included here only one type of list
type='simple'.</para>
<para>
If LIST[1] the variable will default the value at 5mm.</para>
<para>
Since fo:inline does not generate space, XSL Formatter will not generate
an ID if empty. Therefore, I have included a non-printable character.
CAUTION: This character works, but it will appear as a '.'
in the bookmarks.</para>
<para>
I have tested it, but you might find something that needs changing due to a
pure HTML transformation.</para>


	<programlisting>
&lt;xsl:template match="list[@type='simple' and not(child::defitem)]">
  &lt;xsl:param name="nest_list"/>
  &lt;!-- children only -->
    &lt;!-- head; (NOT allowed lcpractices) -->
    &lt;!-- item; -->
    &lt;!-- listhead; defitem; -->
    &lt;!-- @id; -->
    &lt;!-- @continuation /continues; starts; -->
    &lt;!-- @numberation /arabic; -->
    &lt;!--              /upperalpha; loweralpha; -->
    &lt;!--              /upperroman; lowerroman; -->
    &lt;!-- @type /simple; deflist; marked; ordered; -->
    &lt;!--       /simple 'no numbers or bullets' -->
    &lt;!--       /deflist = defitem/label; item; --> 
    &lt;!--       /marked = see @mark -->
    &lt;!--       /ordered = 'each list item lettered or numbered' -->
    &lt;!-- @mark /'bullet, box, dash, or etc.' -->
  &lt;xsl:if test="string-length(@id)&amp;gt;0">
    &lt;fo:inline id="{@id}">&amp;amp;#x200B;&lt;/fo:inline>
  &lt;/xsl:if>
  &lt;fo:list-block provisional-label-separation="5mm">
    &lt;xsl:attribute name="provisional-distance-between-starts">
      &lt;xsl:choose>
        &lt;xsl:when test="string-length($nest_list)!=0">
          &lt;xsl:value-of select="number($nest_list)"/>
          &lt;xsl:text>mm&lt;/xsl:text>
        &lt;/xsl:when>
        &lt;xsl:otherwise>
          &lt;xsl:text>5mm&lt;/xsl:text>
        &lt;/xsl:otherwise>
      &lt;/xsl:choose>
    &lt;/xsl:attribute>
    &lt;xsl:apply-templates select="*[not(name()='head')]">
      &lt;xsl:with-param name="nest_list">
        &lt;xsl:choose>
          &lt;xsl:when test="string-length($nest_list)=0">
            &lt;xsl:number value="5"/>
          &lt;/xsl:when>
          &lt;xsl:otherwise>
            &lt;xsl:value-of select="number($nest_list)"/>
          &lt;/xsl:otherwise>
        &lt;/xsl:choose>
      &lt;/xsl:with-param> 
    &lt;/xsl:apply-templates>
  &lt;/fo:list-block>
  &lt;xsl:apply-templates select="head" mode="non_children"/>
  &lt;xsl:apply-templates select="text()" mode="kill_txt"/> &lt;/xsl:template>

&lt;xsl:template match="item[parent::list[@type='simple']]">
  &lt;xsl:param name="nest_list"/>
  &lt;!-- mixed=true -->
    &lt;!-- archref; bibref; extptr; extref; linkgrp; ptr; ref; -->
    &lt;!-- emph; lb; blockquote; -->
    &lt;!-- address; abbr; date; expan; note; num; title; -->
    &lt;!-- corpname; famname; function; genreform; geogname; -->
    &lt;!-- name; occupation; persname; subject; -->
    &lt;!-- origination; repository; unidate; unittitle; -->
    &lt;!-- chronlist; list; table; -->
    &lt;!-- @id; -->
    &lt;fo:list-item>
      &lt;fo:list-item-label end-indent="label-end()">
        &lt;xsl:attribute name="start-indent">
          &lt;xsl:choose>
            &lt;xsl:when test="string-length($nest_list)=0">
              &lt;xsl:text>5mm&lt;/xsl:text>
            &lt;/xsl:when>
            &lt;xsl:otherwise>
              &lt;xsl:value-of select="number($nest_list)"/>
              &lt;xsl:text>mm&lt;/xsl:text>
            &lt;/xsl:otherwise>
          &lt;/xsl:choose>
        &lt;/xsl:attribute>
        &lt;xsl:if test="number($nest_list)=5">
          &lt;fo:block>&amp;amp;#x2022;&lt;/fo:block>
        &lt;/xsl:if>
      &lt;/fo:list-item-label>
      &lt;fo:list-item-body start-indent="body-start()">
        &lt;fo:block>
          &lt;xsl:if test="string-length(@id)&amp;gt;0">
            &lt;xsl:attribute name="id">
              &lt;xsl:value-of select="@id"/>
            &lt;/xsl:attribute>
          &lt;/xsl:if>
          &lt;xsl:apply-templates select="text() | *[not(name()='list') and
not(name()='chronlist') and not(name()='table')]"/>
        &lt;/fo:block>
        &lt;xsl:apply-templates select="list | chronlist | table">
          &lt;xsl:with-param name="nest_list">
            &lt;xsl:choose>
              &lt;xsl:when test="string-length($nest_list)=0">
                &lt;xsl:number value="5"/>
              &lt;/xsl:when>
              &lt;xsl:otherwise>
                &lt;xsl:value-of select="number($nest_list) +5"/>
              &lt;/xsl:otherwise>
            &lt;/xsl:choose>
          &lt;/xsl:with-param> 
        &lt;/xsl:apply-templates>
      &lt;/fo:list-item-body>
    &lt;/fo:list-item>
&lt;/xsl:template>
</programlisting>

      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Nested list example.</para>
      </question>
      <answer>
	<para role="author">Mike Ferrando</para>


<para>My goals:</para>
<para>
1. create a variable that was only increased by the presence of a nested
LIST element </para>
<para>
2. the syntax should be exactly the same (LIST or ITEM) to
increase the variable when there is a nested list (recursive) </para>

<para>3. LIST/LIST
does not receive bullets (or whatever) based on the variable value. This is
a local preference. I have included here only one type of list
type='simple'.</para>

<para>4. if LIST[1] the variable will default the value at 5mm.</para>
<para>
5. since fo:inline does not generate space, XSL Formatter will not generate
an ID if empty. Therefore, I have included a non-printable character.
CAUTION: This character works, but it will appear as a '.'
in the bookmarks.</para>

	<programlisting>
&lt;xsl:template match="list[@type='simple']">
  &lt;xsl:param name="nest_list"/>
  &lt;fo:list-block provisional-label-separation="5mm">
    &lt;xsl:attribute name="provisional-distance-between-starts">
      &lt;xsl:choose>
        &lt;xsl:when test="string-length($nest_list)!=0">
          &lt;xsl:value-of select="number($nest_list)"/>
          &lt;xsl:text>mm&lt;/xsl:text>
        &lt;/xsl:when>
        &lt;xsl:otherwise>
          &lt;xsl:text>5mm&lt;/xsl:text>
        &lt;/xsl:otherwise>
      &lt;/xsl:choose>
    &lt;/xsl:attribute>
    &lt;xsl:apply-templates select="*[not(name()='head')]">
      &lt;xsl:with-param name="nest_list">
        &lt;xsl:choose>
          &lt;xsl:when test="string-length($nest_list)=0">
            &lt;xsl:number value="5"/>
          &lt;/xsl:when>
          &lt;xsl:otherwise>
            &lt;xsl:value-of select="number($nest_list)"/>
          &lt;/xsl:otherwise>
        &lt;/xsl:choose>
      &lt;/xsl:with-param> 
    &lt;/xsl:apply-templates>
  &lt;/fo:list-block>
  &lt;xsl:apply-templates select="head" mode="non_children"/>
  &lt;xsl:apply-templates select="text()" mode="kill_txt"/> &lt;/xsl:template>

&lt;xsl:template match="item[parent::list[@type='simple']]">
  &lt;xsl:param name="nest_list"/>

    &lt;fo:list-item>
      &lt;fo:list-item-label end-indent="label-end()">
        &lt;xsl:attribute name="start-indent">
          &lt;xsl:choose>
            &lt;xsl:when test="string-length($nest_list)=0">
              &lt;xsl:text>5mm&lt;/xsl:text>
            &lt;/xsl:when>
            &lt;xsl:otherwise>
              &lt;xsl:value-of select="number($nest_list)"/>
              &lt;xsl:text>mm&lt;/xsl:text>
            &lt;/xsl:otherwise>
          &lt;/xsl:choose>
        &lt;/xsl:attribute>
        &lt;xsl:if test="number($nest_list)=5">
          &lt;fo:block>&amp;amp;#x2022;&lt;/fo:block>
        &lt;/xsl:if>
      &lt;/fo:list-item-label>
      &lt;fo:list-item-body start-indent="body-start()">
        &lt;fo:block>
          &lt;xsl:if test="string-length(@id)&amp;gt;0">
            &lt;xsl:attribute name="id">
              &lt;xsl:value-of select="@id"/>
            &lt;/xsl:attribute>
          &lt;/xsl:if>
          &lt;xsl:apply-templates select="text() | *[not(name()='list') 
          and not(name()='table')]"/>
        &lt;/fo:block>
        &lt;xsl:apply-templates select="list | chronlist | table">
          &lt;xsl:with-param name="nest_list">
            &lt;xsl:choose>
              &lt;xsl:when test="string-length($nest_list)=0">
                &lt;xsl:number value="5"/>
              &lt;/xsl:when>
              &lt;xsl:otherwise>
                &lt;xsl:value-of select="number($nest_list) +5"/>
              &lt;/xsl:otherwise>
            &lt;/xsl:choose>
          &lt;/xsl:with-param> 
        &lt;/xsl:apply-templates>
      &lt;/fo:list-item-body>
    &lt;/fo:list-item>
&lt;/xsl:template>
</programlisting>


      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Mimic a list using side floats</para>
      </question>
      <answer>
	<para role="author">G. Ken Holman</para>




<para>I hope this example helps:</para>

	<programlisting>   &lt;fo:block 
 xmlns="http://www.w3.org/1999/XSL/Format" font-size="20pt">
     &lt;fo:block>Mimic a list using side floats&lt;/fo:block>
     &lt;fo:block intrusion-displace="block">
       &lt;fo:float 
 float="start">&lt;fo:block>1.1.1&amp;#xa0;&lt;/fo:block>&lt;/fo:float>
       Here is a long body with a short label to the start
       side of the page with some wrapping going
       on to show the effect on the label.
     &lt;/fo:block>
     &lt;fo:block intrusion-displace="block">
       &lt;fo:float 
  float="start">&lt;fo:block>1.1.1.1.1.1&amp;#xa0;&lt;/fo:block>&lt;/fo:float>
       Here is a long body with a longer label to the start
       side of the page with some wrapping
       going on to show the effect on the label.
     &lt;/fo:block>
   &lt;/fo:block>
</programlisting>


      </answer>
    </qandaentry>

</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
