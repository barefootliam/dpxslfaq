<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-10-23 05:59:50 dpawson"   -->

<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="cdata">
 <config param="desc" value="cdata"/>
 <config param="dir" value="xsl"/>
 <config param="filename" value="cdata.html"/>
 <head>
  <title>CDATA Sections</title>
  <summary>CDATA</summary>
 </head>
 <qandaset>

 <qandaentry>
      <question>
	<para>CDATA sections</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
<para>	
I am trying to transform xml to xml and the end schema
dictates that I have to put all of my actual content
(HTML page) into an element like this:</para>

	<programlisting>&lt;property name="body">&lt;[[CDATA[ potentially nasty,
ill-formed HTML ]&amp;#x5d;>&lt;/property>
</programlisting>


<para>cdata-section-elements takes a whitespace-separated list of element
names, e.g.</para>

	<programlisting>cdata-section-elements="a b c d e"
</programlisting>      </answer>
    </qandaentry>

  <qandaentry>
   <question>
    <para>Understanding CDATA </para>
   
   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
 <literallayout format="linespecific">
>I have this Javascript in my xsl :
>
>&lt;script language="JavaScript">
>&lt;![CDATA[function image(){
>         window.open('  NOT YET DEFINED ');
>}
>&#x5D;]>  
>&lt;/script>
>
>I would like to know how to make the method "WINDOW.OPEN" work with a 
>variable that would get the information which is stored in the IMAGE 
>element of my xml, so putting the image in the opening window. I will have 
>340 different xml pages, one for each expression of the encyclopedia.
</literallayout>
    <para>




From what I gather, you are generating this script element as part of the
output HTML from a stylesheet.  Just as with any other output that you
generate within an XSL page, you can insert values of particular XPath
expressions using xsl:value-of.  So if you want the value of the @href
attribute of the IMAGE element, you can use:</para>


     <programlisting>&lt;xsl:value-of select="/ROOT/SEE_PICTURE/IMAGE/@href" /></programlisting>
   

<para>I'm sure you're aware of this, so I guess that the problem you're having is
inserting this into the javascript.  If you were to simply insert it into
your existing script element like:
</para>

    <programlisting>&lt;script language="JavaScript">
  &lt;![CDATA[
  function image() {
    window.open('&lt;xsl:value-of select="/ROOT/SEE_PICTURE/IMAGE/@href" />');
  }
  ]&rsqb;>
&lt;/script>
</programlisting>

<para>then it would be within a CDATA section, and would thus not be interpreted
as XSLT (or XML for that matter).
</para>

<para>Within the XSLT stylesheet, the CDATA section is purely a utility to stop
you from having to escape all the '&lt;' etc. that you would have to
otherwise.  The CDATA section in your XSLT stylesheet does not translate
into a CDATA section in your output.  So your script element translates to:
</para>

    <programlisting>&lt;script language="JavaScript">
  function image() {
    window.open('  NOT YET DEFINED ');
  }
&lt;/script>
</programlisting>

<para>as there are no peculiar characters to be escaped within it.  Given that,
you could simply do:
</para>

    <programlisting>&lt;script language="JavaScript">
  function image() {
    window.open('&lt;xsl:value-of select="/ROOT/SEE_PICTURE/IMAGE/@href" />');
  }
&lt;/script>
</programlisting>

<para>and the value of the IMAGE's @href will be inserted as an argument.  If
there *are* characters that need to be escaped in your javascript, then
there's no problem just stopping the CDATA section before the piece of XSLT
and then starting it again afterwards:
</para>

    <programlisting>&lt;script language="JavaScript">
  &lt;![CDATA[
  function image() {
    window.open(']&rsqb;>&lt;xsl:value-of
                      select="/ROOT/SEE_PICTURE/IMAGE/@href" />&lt;![CDATA[');
  }
  ]&rsqb;>
&lt;/script>
</programlisting>


    <para>Jeni then carries on answering another question...</para>

<para>CDATA sections are designed to take the trauma out of escaping text that
has lots of characters that need escaping in (like &lt; and &amp;).  Putting
&lt;![CDATA[...]&rsqb;> round a section essentially says "do not parse this section
- - everything in it should be interpreted as characters".
</para>
    <para>So, in the example:</para>

     <programlisting>&lt;![CDATA[
  &lt;img src="global.gif" alt="Go around the world" />
]&rsqb;></programlisting>

	<para>is exactly the same as:</para>

     <programlisting>  &amp;lt;img src="global.gif" alt="Go around the world" /></programlisting>

<para>The XML parser sees a string, not a tag.  The XSLT processor therefore sees
a string, not a tag, and processes it as a string, not a tag, which means
that it outputs:
</para>
    <programlisting>  &amp;lt;img src="global.gif" alt="Go around the world" /></programlisting>

<para>which gets displayed in your browser.
</para>
<para>The goal you're aiming for is copying something that you have in your XML
source directly into your HTML output.  The xsl:copy-of element is designed
precisely for this purpose.  xsl:copy-of will give an exact copy of
whatever you select, including attributes and content.
</para>
<para>So, you can just have XML like:
</para>
    <programlisting>&lt;page>
  We offer the cheapest air fares to Bombay.
  &lt;img src="global.gif" alt="Go around the world" />
&lt;/page>
</programlisting>
<para>And then a template that says 'when you come across an img element, just
copy it':
</para>
<programlisting format="linespecific">&lt;xsl:template match="img">
  &lt;xsl:copy-of select="." />
&lt;/xsl:template>
</programlisting>
<para>If you have lots of HTML that you want to copy straight over from your XML
source to your HTML output, the cleanest approach is to define an 'html'
namespace and mark all the HTML elements as belonging to this namespace:
</para>
    <programlisting>&lt;page xmlns:html="http://www.w3.org/1999/xhtml">
  We offer the cheapest air fares to Bombay.
  &lt;html:img src="global.gif" alt="Go around the world" />
&lt;/page>
</programlisting>
<para>To copy these HTML elements whenever you come across them within your page,
have a template that matches them (html:* - any element in the html
namespace) and copies them:
</para>
    <programlisting>&lt;xsl:template match="html:*">
  &lt;xsl:copy-of select="." />
&lt;/xsl:template>
</programlisting>




   </answer>
  </qandaentry>


    <qandaentry>
   <question>
    <para>CDATA Help</para>

   </question>
   <answer>
    <para role="author">Mike Brown </para>
    <programlisting format="linespecific">


> Can someone please explain to me why the following :
> 
>     &lt;![CDATA[&lt;BR/>&#x005d;;]>
> 
> ... gets converted to the following when output is set to html :
> 
>     &amp;lt;BR/>
	</programlisting>
<para>CDATA sections in an XML document serve no other purpose than to
unambiguously say "this is all text, not markup". In practice, all it does
is it keeps you from having to escape the beginning-of-markup characters
'&lt;' and '&amp;' and on occasion the end-of-markup '>'.</para>
<para>
You are under the mistaken impression that in a CDATA section '&lt;' and '&amp;'
mean something different than '&amp;lt;' and '&amp;amp;' would mean outside of a
CDATA section, but they do not; an XML parser will treat them the same.</para>
<para>
To further clarify, this XML:</para>

	<programlisting>	&lt;p>hello&lt;BR/>world&lt;/p>

implies an XPath/XSLT node tree like this:

	element 'p'
	  |___text 'hello'
	  |___element 'BR'
	  |___text 'world'
</programlisting>
<para>While this XML:</para>
	<programlisting>
	&lt;p>hello&amp;lt;BR/&amp;gt;world&lt;/p>

or this XML:

	&lt;p>&lt;![CDATA[hello&lt;BR/>world&#x5d;;]>&lt;/p>
</programlisting>
<para>are logically equivalent, implying a node tree like this:</para>

	<programlisting>	element 'p'
	  |___text 'hello&lt;BR/>world'
</programlisting>
<para>If you have that in your result tree and you emit it as XML, the
serializer will make start and end tags as appropriate for the nodes,
embedding and quoting attribute nodes in the tags as appropriate. The last
fragment above would very likely be emitted as
&lt;p>hello&amp;lt;BR/&amp;gt;world&lt;/p>, although it wouldn't be wrong to emit it as
&lt;p>&lt;![CDATA[hello&lt;BR/>world&#x5d;;]>&lt;/p>. You certainly wouldn't want to get
output of &lt;p>hello&lt;BR/>world&lt;/p> because we've already established that
this means something completely different.</para>
<para>
HTML output is very similar, the only real difference in this case being
that empty elements will be represented by what looks like a start tag
only, like &lt;BR> instead of &lt;BR/> ... but that's assuming you've got a BR
*element* in your result tree.</para>
<para>
So you are hereby challenged to get a BR element into the result tree, so
it will be serialized as an actual BR tag by the HTML outputter. Three
ways to do it:</para>
	<programlisting>
1. &lt;BR/> literal result element in the stylesheet
2. &lt;xsl:element name="BR"/> instruction in the stylesheet
3. &lt;xsl:copy-of select="/path/to/an/empty/BR/element/in/source/tree"/>
   instruction in the stylesheet
</programlisting>
<para>All well and good, but you said you wanted &lt;BR/>, which is *not HTML*. Why
then are you relying on the HTML output method? Tsk. Only one way then,
the wrong way, despised because</para>

	  <simplelist>
	    <member>people who rely on it tend to produce malformed documents</member>
	    <member>people who rely on it think in terms of tags-in-tags-out, when
   they should be thinking about the information set implied by an XML
   document, the trees based on that information, and the automatic
   derivation of output from the new trees constructed based on the
   stylesheet contents...</member></simplelist>




	<programlisting>&lt;xsl:value-of select="'&amp;lt;BR/&amp;gt;'" disable-output-escaping="yes"/>
</programlisting>
<para>Hey, you asked for verbosity.</para>



   </answer>
  </qandaentry>


 

 
  <qandaentry>
   <question>
    <para>Can I test for a CDATA section?</para>

   </question>
   <answer>
    <para role="author">Mike Kay</para>
    <para>
No, text in a CDATA section looks exactly the same to the XSLT processor as
text outside a CDATA section. For example,
    </para>

    <programlisting>&lt;![CDATA[xxx]&rsqb;> looks exactly like xxx
&lt;![CDATA[&lt;>]&rsqb;> looks exactly like &amp;lt;&amp;gt;
</programlisting>

    <para>This is because they are intended to be equivalent ways of writing the same
thing.
</para>

   </answer>
  </qandaentry>

 
   <qandaentry>
   <question>
    <para>Copying CData sections from the source to destination documents</para>

   </question>
   <answer>
    <para role="author">David Carlisle</para>
    <programlisting format="linespecific">

> So, is there a way that I can specify that a node and its descendants
> should be copied 'as is'?
	</programlisting>
<para>No.</para>
<para>
CDATA sections (like entity references) are not considered to be part of
the document tree in an XPath processor. They are just an authoring
convenience. Not seeing CDATA markup is like not being able to tell if "
or ' were used around the attribute values in the original document.</para>
	<programlisting>
&lt;![CDATA[&amp;&amp;&amp;&amp;&amp;&amp;&#x5d;]>

produces an _identical_ input tree to

&amp;amp;&amp;amp;&amp;amp;&amp;amp;&amp;amp;&amp;amp;

so there is no way that XSL can distinguish these.
</programlisting>
<para>What you can do is request that certain elements are always output using
CDATA sections.</para>
<para>
Mike Kay adds:</para>
<para>
You can easily copy it to an output file that is equivalent, but not to one
that is lexically identical. The process of parsing the source XML to
produce the XSLT tree loses lexical details such as entity boundaries, CDATA
sections, order of attributes, and whitespace within tags. See XSLT Prog Ref
page 63.</para>
<para>
Does it matter whether the output uses character references or CDATA? They
are just different ways of expressing the same information.</para>

   </answer>
  </qandaentry>



 

 <qandaentry>
   <question>
    <para>Displaying document( ) output within CDATA</para>

   </question>
   <answer>
    <para role="author">Adam Turoff</para>
     <literallayout format="linespecific" class="normal">

> Any suggestions on how to insert code from an external
> file into a CDATA section would be appreciated.
	</literallayout>
<para>
If you want to emit a </para>
	<programlisting>&amp;lt;![CDATA[ ... &#x5d;]> </programlisting>
	<para>section on output, you probably
want to look at</para>
	<programlisting> &amp;lt;xsl:output cdata-section-elements=""/>. </programlisting>
	<para> This will make
the child text content of an element appear within a CDATA section.  You
can hack CDATA sections by hand to appear in the middle of a stream
of text nodes, but it involves hacking around the output escaping of &amp;lt; and >.</para>
<para>
In your case, if you want to show examples as-is, as they are found
in an external XML file (or XML fragment), you probably want to use </para>
	<programlisting>
	  &amp;lt;xsl:copy-of select=document()"/></programlisting>
<para>
Here's a stylesheet fragment that may help you get going.</para>

	<programlisting>
&lt;xsl:output method="xml" cdata-section-elements="example"/>

&lt;xsl:template match="example">
&lt;example>
&lt;xsl:copy-of select="document(@href)"/>
&lt;/example>
&lt;/xsl:template></programlisting>

<para>That will select the document specified by </para>
	<programlisting>&lt;example href=""/></programlisting>
	<para> in your
source, and display it as </para>
	<programlisting>&lt;example>&lt;![CDATA[ ... &#x5d;]>&lt;/example> </programlisting>
	<para>in your
output.</para>


   </answer>
  </qandaentry>


 <qandaentry>
   <question>
    <para>CDATA sections in the output</para>

   </question>
   <answer>
    <para role="author">Tom Passin</para>
     <literallayout format="linespecific" class="normal">
 
  > I need to transform an  XML file into
  > a different XML file. one element needs to be defined as 
  CDATA in the  second XML file.
  >
  > right now I have
  >
  >    &lt;xsl:template match="Action">
  >       &lt;solution>&lt;xsl:apply-templates select="text()|i|b" />
  >       &lt;/solution>
  >    &lt;/xsl:template>
  >
  > which produces an output like
  >
  >    &lt;solution>this is a &lt;i>nice&lt;/i> test&lt;/solution>
  >
  > but now i need the output to be a CDATA section, as i don't want the
  > &lt;i>&lt;/i> part being parsed further, eg the output must look like
  >
  >    &lt;solution>&lt;![CDATA[this is a &lt;i>nice&lt;/i> test&#x5d;]>&lt;/solution>
  >
</literallayout>

  <para>This is a job for the cdata-section-elements attribute of 
  xsl:output.  You
  supply a (whitespace-separated) list of elements for which 
  you want text
  content to be wrapped in CDATA sections.</para>
   </answer>
  </qandaentry>

    <qandaentry>
      <question>
	<para>Showing source code program listings</para>
      </question>
      <answer>
	<para role="author">Mike Brown</para>
	<literallayout>
> can anybody give me a hint, how I can store code-listings 
> (java, c++, ....)
> in my xml-file and put it out as html with XSLT?
> 
> It's just for showing some code-chunks on the website.
> 
> I tried to use &lt;![CDATA[  ... code in here ]> 
> but this doesn't work properly.
> I even need do recognize LF to format the output.
</literallayout>
<para>You can use CDATA sections in the XML, that's fine. Or you can replace all
the
"&lt;" with "&amp;lt;" and "&amp;" with "&amp;amp;", which is all a CDATA section saves you
from having to do...</para>
	<programlisting>
&lt;code>&lt;![CDATA[

   text and &lt;tags> &amp; stuff

]&amp;#x5D;>&lt;/code>
</programlisting>
<para>is the same as</para>
	<programlisting>
&lt;code>

   text and &amp;lt;tags> &amp;amp; stuff

&lt;/code>
</programlisting>
<para>Make sure you have &lt;xsl:output method="html"/>, and copy the parsed
text through...</para>
	<programlisting>
&lt;xsl:template match="code">
  &lt;pre>
    &lt;xsl:value-of select="."/>
  &lt;/pre>
&lt;/xsl:template>
</programlisting>
<para>
Your HTML output will have</para>

	<programlisting>&lt;pre>

    text and &amp;lt;tags> &amp;amp; stuff

&lt;/pre>
</programlisting>
<para>
The fact that it's in a &lt;pre> will cause whitespace to be
preserved when it is rendered by the HTML user agent. The
"&amp;lt;" and "&amp;amp;" are how the HTML serializer in
the XSLT processor decided to output the "&lt;" and "&amp;"
in the parsed data, in order to conform to HTML syntax. It
will be parsed by the HTML browser just like it would in
XML, so you'll get "&lt;" in the rendering, don't
worry. Fine-tune the rendering with CSS. No need to get
fancy with replacing linefeeds with &lt;br>s since &lt;pre>
does all the work for you.</para>

<para>Michael Kay offers</para>

<para>Hold it in the XML file in a CDATA section, and output it in an HTML pre
element.</para>


      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>CDATA, How to preserve boundaries after XSL transformation</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>

<para>
your requirements are incompatible with using XSLT.</para>
<para>
to XSLT </para>

<programlisting>a &lt;![CDATA[ 1 &lt; 2 &#x5d;]> b</programlisting>
<para>
is _identical_ input to</para>
<programlisting>
a 1 &amp;lt; 2 b</programlisting>
<para>
CDATA is just a syntactic alternative to using references, and is not recorded in the input tree, just as &lt;a b="2"/> is the same as &lt;a
b   =   '2'   /> produce identical input and you can not preserve white
space and quote styles inside tags.</para>


      </answer>
    </qandaentry>


 <qandaentry>
      <question>
	<para>How can I pass CDATA sections through to the output of a transform</para>
      </question>
      <answer>
	<para role="author">Andrew Welch</para>
	<literallayout>
Bottom line, you can't. 
	</literallayout>

<para>But! By capturing input events and modifying the source the
entity information can be put through to the output for further
	    processing. See <ulink url="http://andrewjwelch.com/lexev/">this</ulink> for the detail</para>

	  <simplelist>
	    <member>Converting cdata sections into markup</member>
	    <member>Preserving entity references</member>
	    <member>Preserving the doctype declaration</member>
	    <member>Marking up comments</member>
	  </simplelist>
	  <para>Quite versatile.</para>

      </answer>
    </qandaentry>



</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
