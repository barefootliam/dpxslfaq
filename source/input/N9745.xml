<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2003-06-15 12:29:27 dpawson"  -->
<!DOCTYPE webpage SYSTEM "../docbook/website/schema/dtd/website.dtd">
<webpage navto="yes" id="N9745">
<head>
<title>JavaScript</title>
<summary>JavaScript</summary>
      <keywords>xslt, javascript</keywords>
      <description>xslt and javascript</description>
</head>
<qandaset>



 <qandaentry>
   <question>
    <para>JavaScript in an XSL file</para>

   </question>
   <answer>
    <para role="author">Mike Brown</para>
    <programlisting format="linespecific">


> I have an XSL file that I use to display my XML data. The XSL file contains
> some JavaScript. How can
> I get XSL to understand that this is code and not some tagging? 
> I have defined my
> script block with LANGUAGE="JavaScript" inside the HEAD block which is
> inside the &lt;xsl:template match="/"> block.
> 
> I have gotten it to work when I replace the &lt; sign with &amp;lt; which is pretty
> ugly!
	</programlisting>
<para>Ugliness aside, it is absolutely correct.</para>
<para>
You don't have the option of making your XSLT be malformed XML. So you
*must* escape it, or use a &lt;![CDATA[ ... &#x05d;]> section, which serves no
purpose but to make it so you don't have to escape the text in that
section. This escaping is input-side only; it has no bearing on what the
character data actually means, nor on the output after XSLT processing.</para>
<para>
Having unescaped markup characters in 'script data' type elements in HTML
is an option offered by HTML for the convenience of document authors --
the 'natural' way is to escape every markup character that's not being
used as markup. HTML is nice about it and says that unescaped markup
character in certain elements is required to be treated exactly the same
as an escaped markup character. That's why you can get away with not
escaping it.</para>
<para>
A stylesheet is not a literal specification for output. Escape the
not-being-used-as-markup markup characters on the way in, and let the
processor escape them on the way back out. Internally, they aren't escaped
at all, but you wouldn't know that by looking at just the input and
output.</para>
<para>
If you feel that the text nodes your stylesheet is creating must be
serialized without markup characters being escaped (which could create
malformed output, if you think about it), then you have the option of
using the disable-output-escaping="yes" attribute on xsl:value-of or
xsl:text instructions that create the text nodes. I would not worry about
it, though. Browsers are required to handle escaped script data.</para>

   </answer>
  </qandaentry>


<qandaentry>
    <question>
<para>Incorporating javascript into a stylesheet</para>
</question>
    
    
      
    
    <answer>
<para role="author">Ben Robb</para>
      <programlisting>
You may have a xsl page something like:

&lt;xsl:stylesheet ...&gt;
&lt;xsl:template match="/"&gt;
&lt;html&gt;
 &lt;head&gt;
 &lt;script language="Javascript"&gt;&lt;![CDATA[
	function myAlert() {
  		window.status="Welcome to my homepage";
	}
 ]]&gt;&lt;/script&gt;
 &lt;/head&gt;
 &lt;body&gt;
&lt;xsl:for-each select="main_news/news/title"&gt;
	&lt;a href="Javascript:myAlert()"&gt;
	&lt;li/&gt;&lt;xsl:value-of select="./@headline"/&gt;&lt;/a&gt;
		&lt;/xsl:for-each&gt;
 &lt;/body&gt;
&lt;/html&gt;
&lt;/xsl:template&gt;
&lt;/xsl:stylesheet&gt;
</programlisting>
    </answer>
  </qandaentry>



  <qandaentry>
   <question>
    <para> Scripting from XSLT</para>

   </question>
   <answer>
    <para role="author">Chris Bayes.</para>
    <para>

This example creates a script block with a namepace prefix of "user" that
contains a function called "xml" that takes a node-list as an argument.
Later, this function is called from the select attribute of &lt;xsl:value-of>.
    </para>

    <programlisting>&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:user="http://mycompany.com/mynamespace"
                version="1.0">
  &lt;msxsl:script language="JScript" implements-prefix="user">
    function xml(nodelist) {
      return nodelist.nextNode().xml;
    }
  &lt;/msxsl:script>

  &lt;xsl:template match="/">
    &lt;xsl:value-of select="user:xml(.)"/>
  &lt;/xsl:template>
&lt;/xsl:stylesheet>

</programlisting>

   </answer>
  </qandaentry>


 <qandaentry>
   <question>
    <para>including javascript</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>

	<programlisting>
>	&lt;xsl:element name="A">
>		&lt;xsl:attribute name="href">
>
javascript:Popup=window.open('','IntlPopup','alwaysRaised=1,dependent=1,
height=300,location=0,menubar=1,personalbar=0,scrollbars=0,status=0,
toolbar=0,width=590,resizable=0');
>			Popup.focus();
>			Popup.document.write('
>			&lt;TABLE>
>			&lt;xsl:copy-of select="tr"/>
>			&lt;/TABLE>
>			');
>		&lt;/xsl:attribute>
>			Enlarge this table
>	&lt;/xsl:element>
</programlisting>
<para>If you look at the code above, you'll see that you're setting the @href
attribute on the generated A element to a string inside which (as far as
the XSLT processor's concerned) you're creating a TABLE element, and a
number of copies of other elements as well.
</para>
<para>XSLT processors justifiably won't let you put elements within attribute
content: you have to escape the &lt; and >.  This means that you can't simply
copy the tr elements across - you have to output them as serialised XML.  I
wrote this template earlier today for a similar problem:
</para>
	<programlisting>&lt;xsl:template match="*" mode="serialise">
  &lt;xsl:text />&amp;lt;&lt;xsl:value-of select="name()" />
  &lt;xsl:for-each select="@*">
    &lt;xsl:text> &lt;/xsl:text>
    &lt;xsl:value-of select="name()" />
    &lt;xsl:text />="&lt;xsl:value-of select="." />"&lt;xsl:text />
  &lt;/xsl:for-each>
  &lt;xsl:text>&amp;gt;&lt;/xsl:text>
  &lt;xsl:apply-templates mode="serialise" />
  &lt;xsl:text />&amp;lt;/&lt;xsl:value-of select="name()" />&amp;gt;&lt;xsl:text />
&lt;/xsl:template>
</programlisting>
<para>So, you can either replace the above with:
</para>
	<programlisting>	&lt;xsl:element name="A">
		&lt;xsl:attribute name="href">
javascript:Popup=window.open
('','IntlPopup','alwaysRaised=1,dependent=1,height=300,
location=0,menubar=1,personalbar=0,scrollbars=0,
status=0,toolbar=0,width=590,resizable=0');
			Popup.focus();
			Popup.document.write('
			&amp;lt;TABLE&amp;gt;
			&lt;xsl:apply-templates select="tr" mode="serialise" />
			&amp;lt;/TABLE&amp;gt;
			');
		&lt;/xsl:attribute>
			Enlarge this table
	&lt;/xsl:element>
</programlisting>
<para>Or you can use CDATA sections so that you don't have to worry about
escaping the &lt; and > around the TABLE element:
</para>
	<programlisting>	&lt;xsl:element name="A">
		&lt;xsl:attribute name="href">

&lt;![CDATA&lsqb;javascript:Popup=window.open
('','IntlPopup','alwaysRaised=1,dependent=1,height=300,location=0,
menubar=1,personalbar=0,scrollbars=0,status=0,toolbar=0,width=590,
resizable=0');
		Popup.focus();
		Popup.document.write('
		&lt;TABLE>]&#x05d;>
		&lt;xsl:apply-templates select="tr" mode="serialise" />
		&lt;![CDATA[&lt;/TABLE>
		');]&#x05d;>
	&lt;/xsl:attribute>
		Enlarge this table
	&lt;/xsl:element>
</programlisting>
<para>The resultant output will be:
</para>
	<programlisting>&lt;A
href="javascript:Popup=window.open
('','IntlPopup','alwaysRaised=1,dependent=1,height=300,location=0,
menubar=1,personalbar=0,scrollbars=0,status=0,toolbar=0,width=590,
resizable=0');
	Popup.focus();
	Popup.document.write('
	&amp;lt;TABLE&amp;gt;
	&amp;lt;tr&amp;gt;...&amp;lt;/tr&amp;gt;
	&amp;lt;/TABLE&amp;gt;
	');">Enlarge this table&lt;/A></programlisting>

<para>but the Javascript processor will see the unescaped string:
</para>
	<programlisting>javascript:Popup=window.open
('','IntlPopup','alwaysRaised=1,dependent=1,height=300,location=0,
menubar=1,personalbar=0,scrollbars=0,status=0,toolbar=0,width=590,
resizable=0');
	Popup.focus();
	Popup.document.write('
	&lt;TABLE>
	&lt;tr>...&lt;/tr>
	&lt;/TABLE>
	');</programlisting>

<para>and therefore 'do the right thing'.  Or should, anyway.
</para>
<para>Additional note from David Carlisle:
</para>
	<programlisting>> But what is the purpose of empty &lt;xsl:text /> instructions?</programlisting>

<para>It's the same as putting the characters inside a non empty xsl:text,
but saves a few keystrokes. Note the end result is the same,
the white space used to indent the stylesheet is in nodes that only have
white space so is stripped.
</para>
	<programlisting>
     &lt;xsl:value-of select="name()" />
     &lt;xsl:text />="&lt;xsl:value-of
</programlisting>
<para>is effectively same as
</para>
	<programlisting>     &lt;xsl:value-of select="name()" />
     &lt;xsl:text>="&lt;xsl:text>&lt;xsl:value-of
</programlisting>
<para>but both of those are not the same as
</para>
	<programlisting>     &lt;xsl:value-of select="name()" />
     ="&lt;xsl:value-of
</programlisting>
<para>which puts a newline and some spaces before the = in the output tree. 
</para>
<para>Mike Kay goes on:
</para>
<para>An empty &lt;xsl:text/> instruction splits a text node into two, typically
making one of them a whitespace-only node that will be stripped from the
stylesheet.
</para>
	<programlisting>Useful e.g. in
&lt;xsl:attribute>
&lt;xsl:text/>[&lt;xsl:value-of select="$x"/>]&lt;xsl:text/>
&lt;/xsl:attribute>
</programlisting>
<para>to prevent newlines in the output. You can't put an &lt;xsl:text> element
around the &lt;xsl:value-of>, it's not allowed to contain child elements, and
the alternative of
</para>
	<programlisting>&lt;xsl:text>[&lt;/xsl:text>&lt;xsl:value-of select="$x"/>&lt;xsl:text>]&lt;/xsl:text>
</programlisting><para>
is even uglier.
</para>

   </answer>
  </qandaentry>


 
   <qandaentry>
   <question>
    <para>Javascript escaping of single quote</para>

   </question>
   <answer>
    <para role="author">Christopher R. Maden</para>
    <programlisting format="linespecific">


>I have some JavaScript functions that, on click of a glossed word, open a
>new window and writes to it with document.write.  That all works fine except
>there is potential for it to break if the defintion contains a single quote.
>Is there some way to apply-templates and search for the single quote
>character and prepend the js escape character "\" to the single quote?
	</programlisting>
	<programlisting>&lt;xsl:template name="fixQuotes">
   &lt;xsl:param name="do.quote"/>
   &lt;xsl:param name="string"/>
   &lt;xsl:choose>
     &lt;xsl:when test="$do.quote">
       &lt;xsl:choose>
         &lt;xsl:when test="contains($string, &amp;quot;'&amp;quot;)">
           &lt;xsl:value-of
             select="substring-before($string, &amp;quot;'&amp;quot;)"/>
           &lt;xsl:text>\'&lt;/xsl:text>
           &lt;xsl:call-template name="fixQuotes">
             &lt;xsl:with-param name="do.quote" select="$do.quote"/>
             &lt;xsl:with-param name="string"
               select="substring-after($string, &amp;quot;'&amp;quot;)"/>
           &lt;/xsl:call-template>
         &lt;/xsl:when>
         &lt;xsl:otherwise>
           &lt;xsl:value-of select="$string"/>
         &lt;/xsl:otherwise>
       &lt;/xsl:choose>
     &lt;/xsl:when>
     &lt;xsl:otherwise>
       &lt;xsl:value-of select="$string"/>
     &lt;/xsl:otherwise>
   &lt;/xsl:choose>
&lt;/xsl:template>
</programlisting>

<para>Chris McGrath  provides a Microsoft Javascript alternative.</para>

	<programlisting>&lt;?xml version='1.0'?>
&lt;xsl:stylesheet version="1.0"
       xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
       xmlns:msxsl="urn:schemas-microsoft-com:xslt"
       xmlns:user="user"
       exclude-result-prefixes="msxsl user">

&lt;xsl:output method="html" indent="no" />

&lt;msxsl:script language="JavaScript" implements-prefix="user">
&lt;![CDATA[
	function encodeString(str_in) {
		return escape(str_in);
	}
]&#x05d;>
&lt;/msxsl:script>

.....

&lt;xsl:template match="Blah">
 &lt;xsl:element name="a">
  &lt;xsl:attribute name="id">&lt;xsl:value-of
select="user:encodeString(string(@name))"/>&lt;/xsl:attribute>
 &lt;/xsl:element>
&lt;/xsl:template>

.......

&lt;/xsl:stylesheet>

</programlisting>



   </answer>
  </qandaentry>


 
   <qandaentry>
   <question>
    <para>Javascript escaping of single quote</para>

   </question>
   <answer>
    <para role="author">Christopher R. Maden</para>
    <programlisting format="linespecific">

>I have some JavaScript functions that, on click of a glossed word, open a
>new window and writes to it with document.write.  That all works fine except
>there is potential for it to break if the defintion contains a single quote.
>Is there some way to apply-templates and search for the single quote
>character and prepend the js escape character "\" to the single quote?
	</programlisting>
	<programlisting>&amp;lt;xsl:template name="fixQuotes">
   &amp;lt;xsl:param name="do.quote"/>
   &amp;lt;xsl:param name="string"/>
   &amp;lt;xsl:choose>
     &amp;lt;xsl:when test="$do.quote">
       &amp;lt;xsl:choose>
         &amp;lt;xsl:when 
	  test="contains($string, &amp;amp;quot;'&amp;amp;quot;)">
           &amp;lt;xsl:value-of
         select="substring-before($string, &amp;amp;quot;'&amp;amp;quot;)"/>
           &amp;lt;xsl:text>\'&amp;lt;/xsl:text>
           &amp;lt;xsl:call-template name="fixQuotes">
             &amp;lt;xsl:with-param name="do.quote" select="$do.quote"/>
             &amp;lt;xsl:with-param name="string"
        select="substring-after($string, &amp;amp;quot;'&amp;amp;quot;)"/>
           &amp;lt;/xsl:call-template>
         &amp;lt;/xsl:when>
         &amp;lt;xsl:otherwise>
           &amp;lt;xsl:value-of select="$string"/>
         &amp;lt;/xsl:otherwise>
       &amp;lt;/xsl:choose>
     &amp;lt;/xsl:when>
     &amp;lt;xsl:otherwise>
       &amp;lt;xsl:value-of select="$string"/>
     &amp;lt;/xsl:otherwise>
   &amp;lt;/xsl:choose>
&amp;lt;/xsl:template>
</programlisting>


<para>
Chris McGrath  provides a Microsoft Javascript alternative.</para>
	<programlisting>
&amp;lt;?xml version='1.0'?>
&amp;lt;xsl:stylesheet version="1.0"
       xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
       xmlns:msxsl="urn:schemas-microsoft-com:xslt"
       xmlns:user="user"
       exclude-result-prefixes="msxsl user">

&amp;lt;xsl:output method="html" indent="no" />

&amp;lt;msxsl:script language="JavaScript" implements-prefix="user">
&amp;lt;![CDATA[
	function encodeString(str_in) {
		return escape(str_in);
	}
&#x05d;]>
&amp;lt;/msxsl:script>

.....

&amp;lt;xsl:template match="Blah">
 &amp;lt;xsl:element name="a">
  &amp;lt;xsl:attribute name="id">&amp;lt;xsl:value-of
select="user:encodeString(string(@name))"/>&amp;lt;/xsl:attribute>
 &amp;lt;/xsl:element>
&amp;lt;/xsl:template>

.......

&amp;lt;/xsl:stylesheet>
</programlisting>

   </answer>
  </qandaentry>

 <qandaentry>
   <question>
    <para>Javascript Inclusion</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
    <para>
You can mix namespaces to use the MSXML 'msxsl:script' extension
element *as well as* all the really cool stuff you get with XSLT 1.0.
	</para>
<para>The MSXML3.0 SDK documentation, which you can get from
msdn.microsoft.com, tells you about what you can do with msxsl:script.
But here's a small example.</para>

	<programlisting>&lt;!-- include in the xsl:stylesheet element:
     (a) the version attribute as usual
     (b) the XSLT namespace declaration as usual
     (c) the MSXSL namespace declaration
     (d) a namespace declaration to identify your functions
     (e) the 'extension-element-prefixes' attribute to give the
         namespace prefixes that indicate extension elements
         (i.e. 'msxsl')
     (f) the 'exclude-result-prefixes' attribute to indicate the
         namespaces that aren't supposed to be part of the result
         tree (i.e. 'foo') -->
&lt;xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:foo="http://www.broadbase.com/foo"
                extension-element-prefixes="msxsl"
                exclude-result-prefixes="foo">
</programlisting>

	<programlisting>&lt;!-- do whatever output you want - you can use full XSLT functionality
     -->
&lt;xsl:output method="html" />
</programlisting>
	<programlisting>&lt;!-- define the Javascript functions that you want to include within
     a msxsl:script element.
     - language indicates the scripting language
     - implements-prefix gives the namespace prefix that you declared
       for your function (i.e. foo) -->
&lt;msxsl:script language="javascript"
              implements-prefix="foo">
              
   &lt;!-- it's often helpful to use a CDATA section so you don't have to
        worry about escaping less-than signs or anything -->
   &lt;![CDATA[

   &lt;!-- define your functions -->
   function today() {
      today = new Date();
      return today.toLocaleString();
   }

   ]&#x05d;>
&lt;/msxsl:script>

&lt;xsl:template match="/">
   &lt;p>
      This page brought to you on
      &lt;!-- call your functions using the prefix that you've used (i.e.
           foo) anywhere you can normally use an XPath function, but
           make sure it's returning the right kind of object -->
      &lt;xsl:value-of select="foo:today()" />.
   &lt;/p>
&lt;/xsl:template>
                
&lt;/xsl:stylesheet>
</programlisting>
<para>When XSLT 1.1 comes along, it's fairly likely that you'll just be able
to do a global search &amp; replace on 'msxsl:script' for 'xsl:script' and
be XSLT 1.1 compliant.</para>
	<programlisting>
> I have to write simple functions in JavaScript like obtaining month
> name from the number of the month,
</programlisting>
<para>Since you mention this: the classic way of doing this in XSLT is to
add some XML defining the months within the stylesheet itself:</para>
	<programlisting>
&lt;foo:months>
   &lt;foo:month>January&lt;/foo:month>
   &lt;foo:month>February&lt;/foo:month>
   &lt;foo:month>March&lt;/foo:month>
   ...
&lt;/foo:months>
</programlisting>
<para>You then get the name of the month by accessing this XML using the
document() function:</para>
	<programlisting>
&lt;xsl:value-of
  select="document('')/*/foo:months/foo:month[$month-number]" />
</programlisting>
<para>As you are using MSXML-specific code anyway, you could alternatively
do this by defining a variable:</para>
	<programlisting>
&lt;xsl:variable name="months">
  &lt;month>January&lt;/month>
  &lt;month>February&lt;/month>
  &lt;month>March&lt;/month>
  ...
&lt;/xsl:variable>
</programlisting>
<para>and retrieving it with an MSXML-specific extension function, i.e.
msxsl:node-set:</para>
	<programlisting>
&lt;xsl:value-of
  select="msxsl:node-set($months)/month[$month-number]" />
</programlisting>
<para>I mention this to compare and contrast with msxsl:script: both are
extensions over and above the functionality of XSLT 1.0, msxsl:script
an extension element and msxsl:node-set() an extension function. To
use them you just have to use a processor that understands them,
declare the relevant namespace, and specify that it's an extension
namespace, and you're all set.</para>

   </answer>
  </qandaentry>




  <qandaentry>
   <question>
    <para>Variable value from Javascript function</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison </para>
    <programlisting format="linespecific">

> please, is there any way to affect an xml variable with the output
> return of a JavaScript function ?
	</programlisting>
<para>Yes, but be aware that Javascript isn't incorporated into XSLT until
XSLT 1.1 or unless you're using MSXML.  The first thing that you need
to do is to declare the function within an msxsl:script element:</para>

	<programlisting>&lt;msxsl:script language="javascript"
              implements-prefix="foo">
   function retrieveData() {
      return true;
   }
&lt;/msxsl:script>
</programlisting>
<para>In order to do this, you need to declare the 'msxsl' namespace
(urn:schemas-microsoft-com:xslt) and your own 'foo' namespace. The
'msxsl' namespace needs to be declared as an extension element prefix
(using the extension-element-prefixes attribute on xsl:stylesheet) and
the 'foo' namespace should be excluded from your result tree (using
the exclude-result-prefixes or extension-element-prefixes attribute on
xsl:stylesheet).</para>
<para>
Once you've declared the Javascript functions like this, you can call
them within any XPath expression using, e.g.:</para>

	<programlisting>  foo:retrieveData()</programlisting>
<para>
So, to set $my_var to have the value returned by the function, use:</para>
	<programlisting>
  &lt;xsl:variable name="my_var" select="foo:retrieveData()" />
</programlisting>
<para>There's nothing in XSLT that interprets the content of an element as
something to run: calls to functions are *always* in attribute values.</para>

   </answer>
  </qandaentry>

  <qandaentry>
   <question>
    <para>Javascript Function with less than sign</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
    <programlisting format="linespecific">

> I had to use some javascript functions that involves
> for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;(x=a[i])&amp;&amp;
>   x.oSrc;i++) x.src=x.oSrc;
	</programlisting>
<para>OK. To understand how to do this, you have to learn to distinguish
between the *physical* document and the *logical* document.  In a
logical document, the above is the string:</para>
	<programlisting>
  for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;
(x=a[1])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
</programlisting>
<para>However, there are certain characters that are special to XML,
especially '&lt;' and '&amp;'.  XML uses entities to escape them when it
writes a physical document: '&lt;' is serialised as '&amp;lt;' and '&amp;' as
'&amp;amp;' for example.  So *physically* the above string can be
represented in XML as:</para>

	<programlisting>  for(i=0;a&amp;amp;&amp;amp;i&amp;lt;a.length
&amp;amp;&amp;amp;(x=a[1])&amp;amp;&amp;amp;x.oSrc;i++) x.src=x.oSrc;
</programlisting>
<para>When an XML parser reads that, it creates the string that's above,
because it unescapes the relevant characters.  You could just as well
replace *every* character in that string with a character entity
reference, and it would make not one jot of difference to the XML
application using the string (e.g. XSLT processor, XML-aware browser).
For example, I could replace all the 'a' with their character entity
reference:</para>
	<programlisting>
  for(i=0;&amp;#x61;&amp;amp;&amp;amp;i&amp;lt;&amp;#x61;
	.length&amp;amp;&amp;amp;(x=&amp;#x61;[1])&amp;amp;&amp;amp;x.
	oSrc;i++) x.src=x.oSrc;
</programlisting>
<para>Because escaping characters leads to long and unreadable XML, there's
a shorthand way of doing the same thing - using CDATA sections.
Within a CDATA section, all characters are treated just as characters.
So another XML version of the string is:</para>

	<programlisting>  &lt;![CDATA[for(i=0;a&amp;&amp;i&lt;a.
	length&amp;&amp;(x=a[1])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;&#x05d;]>
</programlisting>
<para>So what does this all mean for you?  Well, it's got two implications,
one for your XSLT stylesheet and one for the output that it produces.</para>
<para>
Your XSLT stylesheet must be well-formed XML.  So all the special
characters have to be escaped within it, which you can do (as
discussed above) either with entities or with a CDATA section.  If
you're not dynamically generating the Javascript with XSLT, it's
probably worth using a CDATA section for comfort.  So, in your XSLT
stylesheet you need to have something like:</para>
	<programlisting>
  &lt;head>
     &lt;script language="Javascript">
        &lt;![CDATA[
          ...
          for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;
	  (x=a[1])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
          ...
        &#x05d;]>
     &lt;/script>
  &lt;/head>
</programlisting>
<para>You don't want to place it in a comment because comments in your XSLT
stylesheet won't be outputted into your result tree, and you want the
Javascript to show up in the result.</para>
<para>
Since you are outputting HTML, I guess that you are using the 'html'
output method by setting:</para>
	<programlisting>
  &lt;xsl:output method="html" />
</programlisting>
<para>or by having an 'html' element as the top-most element in your result
tree?</para>
<para>
If you are using the HTML output method, then the Javascript will be
outputted *unescaped* as:</para>
	<programlisting>
  &lt;head>
     &lt;script language="Javascript">
          ...
          for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;(x=a[1])
	  &amp;&amp;x.oSrc;i++) x.src=x.oSrc;
          ...
     &lt;/script>
  &lt;/head>
</programlisting>
<para>This is because HTML accepts unescaped text content in script
elements.</para>
<para>
If you are using the XML output method, perhaps because you're
producing XHTML, then the Javascript will be outputted *escaped*
because you're producing well-formed XML, as:</para>
	<programlisting>
  &lt;head>
     &lt;script language="Javascript">
          ...
          for(i=0;a&amp;amp;&amp;amp;i&amp;lt;a.length&amp;amp;
	  &amp;amp;(x=a[1])&amp;amp;&amp;amp;x.oSrc;i++) x.src=x.oSrc;
          ...
     &lt;/script>
  &lt;/head>
</programlisting>
<para>If you want to make that more readable, you can tell the XSLT
processor that you want the script element to have as its content a
CDATA section, using:</para>
	<programlisting>
  &lt;xsl:output method="xml" cdata-section-elements="script" />
</programlisting>
<para>If you do this, a CDATA section will be automatically used within the
script element, so the output will be:</para>
	<programlisting>
  &lt;head>
     &lt;script language="Javascript">
        &lt;![CDATA[
          ...
          for(i=0;a&amp;&amp;i&lt;a.length&amp;&amp;
	  (x=a[1])&amp;&amp;x.oSrc;i++) x.src=x.oSrc;
          ...
        &#x05d;]>
     &lt;/script>
  &lt;/head>
</programlisting>
<para>Some people have suggested using disable-output-escaping. Disabling
output escaping is *hardly ever* necessary, and often dangerous
because it allows you to produce documents that aren't well-formed.
You should avoid it if you can, and you definitely can in this case.</para>

   </answer>
  </qandaentry>



 <qandaentry>
   <question>
    <para>Including javascript in the output file</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">

  > I been brought into an existing project that is XML/XSL based. Even
  > with limited XSL experience, I see a huge maintenance nightmare
  > evolving. According to the consultants working on the project with
  > us, they have found no way to put needed client side JavaScript
  > functions into an include file. So all JavaScript functions are
  > being duplicated on every page. Since the maintenance of this
  > product will fall on my shoulders, I want to find a better solution.
  > Is there a way to include a .js file that the XSLT parser will
  > accept? Thank you for your time.
  </literallayout>
  <para>It's unclear from your description whether you're talking about
  including the Javascript in the *output* of the transformation (which
  I assume is HTML) or in the stylesheet itself (i.e. you're using
  msxsl:script to get extension functions).</para>
<para>  
  If you're generating HTML, then you can link to an external JavaScript
  document by generating a script element in the same way as you would
  if you were writing the HTML document directly. In other words, in
  your XSLT you should have a literal result element that looks
  something like:</para>
  
	<literallayout>    &lt;script language="javascript" type="text/javascript"
            href="script.js" /></literallayout>
  
  <para>The href is likely to be resolved relative to the XML document that's
  being transformed (at a guess), not the stylesheet.</para>
<para>  
  If you're using msxsl:script to define extension functions, then
  they're right, msxsl:script doesn't have the facility to point to an
  external Javascript file. However, you can put the msxsl:script
  element in its own stylesheet (e.g. script.xsl) and then include it
  into as many other stylesheets as you like using xsl:include:</para>
  
	<literallayout>  &lt;xsl:include href="script.xsl" /></literallayout>
  
  <para>Alternatively, you could use XML entities. Wrap the content of the
  Javascript file in a CDATA section (so that you don't have to escape
  all the &lt;s and &amp;s within it), and then declare the entity in the
  stylesheet that you want to use it in:</para>
  
	<literallayout>  &lt;!DOCTYPE xsl:stylesheet [
  &lt;!ENTITY script SYSTEM 'script.js'>
  ]></literallayout>
  
<para>and then use &amp;script; to insert the script into the file as desired.</para>


   </answer>
  </qandaentry>

  <qandaentry>
   <question>
    <para>Inserting javascript</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">
> In xsl, how can I insert a JavaScript? I have tried:
>
> &lt;Script language="JavaScript" src="functions.js">&lt;/script>
>
> But it didn't work. Can someone help?
	</literallayout>

<para>I've found that Internet Explorer doesn't like script elements that
don't have any content (perhaps because with the XML output method
they don't have a close tag). You can get around the problem by
inserting a comment within the script element:</para>

	<programlisting>  &lt;script type="text/javascript" src="functions.js">
    &lt;!-- comment inserted for Internet Explorer -->
  &lt;/script>
</programlisting>
<para>You can generate this script element from XSLT with:</para>
	<programlisting>
  &lt;script type="text/javascript" src="functions.js">
    &lt;xsl:comment> comment inserted for Internet Explorer &lt;/xsl:comment>
  &lt;/script>
</programlisting>
   </answer>
  </qandaentry>
 <qandaentry>
   <question>
    <para>Script element in Internet Explorer</para>

   </question>
   <answer>
    <para role="author">Jeni Tennison</para>
     <literallayout format="linespecific" class="normal">

> In xsl, how can I insert a JavaScript? I have tried:
>
> &lt;Script language="JavaScript" src="functions.js">&lt;/script>
>
> But it didn't work. Can someone help?
	</literallayout>

<para>I've found that Internet Explorer doesn't like script elements that
don't have any content (perhaps because with the XML output method
they don't have a close tag). You can get around the problem by
inserting a comment within the script element:</para>

	<literallayout>  &lt;script type="text/javascript" src="functions.js">
    &lt;!-- comment inserted for Internet Explorer -->
  &lt;/script>
</literallayout>
<para>You can generate this script element from XSLT with:</para>

	<literallayout>  &lt;script type="text/javascript" src="functions.js">
    &lt;xsl:comment> comment inserted for Internet Explorer &lt;/xsl:comment>
  &lt;/script></literallayout>
   </answer>
  </qandaentry>

<qandaentry>
   <question>
    <para>Javascript braces and AVT's</para>

   </question>
   <answer>
    <para role="author">Michael Kay</para>
     <literallayout format="linespecific" class="normal">

> What is the preferred method for including javascript braces, 
> such that
> 
> &lt;body onload="if (true) { alert('foo'); alert('bar') }">
> 
> doesn't get interpreted as an AVT?
	</literallayout>
<para>Write them as "{{" and "}}".</para>
   </answer>
  </qandaentry>



    <qandaentry>
      <question>
	<para>Running msxsl from jscript</para>
      </question>
      <answer>
	<para role="author">Jonathan Marsh</para>
	<programlisting>

/*

  transform.js
  
  Windows Scripting Host file for performing command-line XSL
  transformations.
  
  Parameters:  source-file stylesheet-file output-file
  
  Author: Jonathan Marsh 
  
*/

var args = WScript.arguments;
if (args.length != 3)
  alert("parameters are: source-file stylesheet-file output-file");
else
{
  var ofs = WScript.CreateObject("Scripting.FileSystemObject");

  var scriptpath = ofs.getParentFolderName(WScript.scriptFullName);
  var source = scriptpath + "\\" + args.item(0);
  var stylesheet = scriptpath + "\\" + args.item(1);
  var dest = scriptpath + "\\" + args.item(2);

  var oXML = new ActiveXObject("MSXML.DOMDocument");
  oXML.validateOnParse = false;
  oXML.async = false;
  oXML.load(source);

  var oXSL = new ActiveXObject("MSXML.DOMDocument");
  oXSL.validateOnParse = false;
  oXSL.async = false;
  oXSL.load(stylesheet);

  var oFile = ofs.CreateTextFile(dest);
  oFile.Write(oXML.transformNode(oXSL));
  oFile.Close();
}

</programlisting>
      </answer>
    </qandaentry>

   <qandaentry>
      <question>
	<para>Escape a single quote</para>
      </question>
      <answer>
	<para role="author">Tom Passin</para>


<para>Here is a template I have used to javascript-escape a single-quote.  The
template works its way through the entire string that it is passed as a
parameter.  In xslt 1.0  you have to do this  recursively.  You need to
remember that if you use, say, substring-after, and you get no match,
nothing is returned (you might think you would get the whole string, but
you do not).  Handling those cases adds some complexity.  </para>
<para>
[The slightly odd formatting, with the closing angle brackets at the
start of new lines, is the simplest way I have found to make __sure__
that no unwanted whitespace sneaks into expressions like these.  Experts
may say you can avoid stray whitespace easily without resorting to
tricks like this, but I have been bit too many times trying to format
javascript properly.  This method may be overkill, but it works (there
is __no__ extra whitespace between elements in the template so there is
no way whitespace nodes can accidentally get added).  Yet it is
formatted almost the same as I would normally format the code].</para>
	<programlisting>
&lt;xsl:template name='fixapos'
>&lt;xsl:param name='data'
/>&lt;xsl:variable name='rest' select='substring-after($data,"&amp;apos;")'
/>&lt;xsl:variable name='first'>&lt;xsl:choose>&lt;xsl:when
test='contains($data,"&amp;apos;")'
>&lt;xsl:value-of select='substring-before($data,"&amp;apos;")'
/>&lt;/xsl:when>&lt;xsl:otherwise>&lt;xsl:value-of select='$data'
/>&lt;/xsl:otherwise>&lt;/xsl:choose>&lt;/xsl:variable>&lt;xsl:value-of
select='$first'
/>&lt;xsl:if test='$rest'>\&amp;apos;&lt;xsl:call-template name='fixapos'
>&lt;xsl:with-param name='data'
     select='$rest'/>&lt;/xsl:call-template>&lt;/xsl:if>&lt;/xsl:template>
</programlisting>
      </answer>
    </qandaentry>


</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
