<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2001-06-10 09:44:23 dave"   -->
<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="N4622">

<head>
<title>Images</title>
<summary>Images</summary>
      <keywords>images, graphics, xslt, xml</keywords>
      <description>XML images, graphics in xslt</description>
</head>
<qandaset>
<qandaentry>
        <question>
<para>Displaying images</para>
</question>
    
        
    
        
      
            
    
        
    
        <answer>
<para role="author">Mike Brown</para>
      
            <programlisting>


&gt; I need to display an image based on the filename from the XML 
&gt; file.  I understand that the result tree simply needs to show 
&gt; the html needed to display the file (that being &lt;img 
&gt; src="picture.jpg"&gt;)</programlisting>

    <para>No, the result tree is a node tree (an abstract thing that
exists only as some kind of a data structure in the XSL
processor). XSLT conveniently lets you say how you want that
node tree to be created via a syntax that looks like
XHTML. This syntax is described in the XSLT spec as
"literal result elements". So for example in your
stylesheet you can have
</para>
    <programlisting>&lt;img src="{foo}"/&gt;</programlisting>

    <para>This tells the XSL processor to add an element named
'img' to the result tree, with an attribute named
'src' and a value of the result of evaluating the
XPath expression foo. The XPath expression should be in
curly braces and should return a string or a node that you
want to see the string value of.  String values for each
node type are explained in the XPath spec.
</para>
    <para>If your source XML is like this:
</para>
    <programlisting>&lt;mydata&gt;
  &lt;ImageInfo&gt;
    &lt;Image ID="123" URI="picture.jpg"/&gt;
  &lt;/ImageInfo&gt;
&lt;/mydata&gt;
</programlisting>
    <para>And your context node was the 'ImageInfo' element,
as in the case when you match on it, the XPath expression to
get the 'URI' attribute of the 'Image'
element that is a child of the context node is quite simply
Image/@URI, which you'd put in the curly braces:
</para>
    <programlisting>&lt;xsl:template match="ImageInfo"&gt;
  &lt;img src="{Image/@URI}"/&gt;
&lt;/xsl:template&gt;
</programlisting>
    <para>If you have the XSL processor serialize and emit the result
tree as XML, you'll see in the output
</para>
    <programlisting>&lt;img src="picture.jpg"/&gt;</programlisting>

    <para>And if you have it serialize it as HTML, you'll see
</para>
    <programlisting>&lt;img src="picture.jpg"&gt;
</programlisting>

    
        </answer>
  
    </qandaentry>

  <qandaentry>
   <question>
    <para>Pre-loading images</para>

   </question>
   <answer>
    <para role="author">Mike Kay</para>
    <programlisting format="linespecific">

> Given the XML:
> 
> &lt;?xml version="1.0"?>
> &lt;LeftNavList>
> 	&lt;PreLoadImage>images/image1&lt;/PreLoadImage>
> 	&lt;PreLoadImage>images/image2&lt;/PreLoadImage>
> 	&lt;PreLoadImage>images/image3&lt;/PreLoadImage>
> &lt;/LeftNavList>
> 
> I would like to generate the following html snippet:
> 
> &lt;body bgcolor="#ffffff" onLoad="PreloadImages
> ('images/image1.gif','images/image2.gif','images/image3.gif');">
> 

  

&lt;xsl:template match="LeftNavList">
  &lt;body bgcolor="#ffffff">
    &lt;xsl:attribute name="onLoad">PreloadImages(
       &lt;xsl:for-each select="PreLoadImage">
         '&lt;xsl:value-of select=".">.gif'
         &lt;xsl:if test="position()!=last()">,&lt;/xsl:if>
       &lt;/xsl:for-each>);&lt;/xsl:attribute>
  &lt;/body>
&lt;/xsl:template>
  </programlisting>
<para>You may want to adjust that a bit for whitespace.
</para>

   </answer>
  </qandaentry>

  <qandaentry>
   <question>
    <para>Problem generating dynamic namespace declarations</para>

   </question>
   <answer>
    <para role="author">David Carlisle</para>
    <para>

With saxon, I think that generating a namespace node in the result
based on a variable value you need to do something like</para>


    <programlisting>&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                xmlns:saxon="http://icl.com/saxon"
                   extension-element-prefixes="saxon"
                >

&lt;xsl:output method="xml" indent="yes"/>

&lt;xsl:param name="p" select="'foo'"/>
&lt;xsl:param name="n" select="'http://x/y/z'"/>

&lt;xsl:template match="/">
&lt;xsl:variable name="x">
 &lt;xsl:element name="{$p}:x" namespace="{$n}"/>
&lt;/xsl:variable>
&lt;xxx path="{$p}:that">
  &lt;xsl:for-each select="saxon:node-set($x)/*/namespace::*[name()=$p]">
  &lt;xsl:copy/>
  &lt;/xsl:for-each>
&lt;!--
  &lt;xsl:copy-of select="saxon:node-set($x)/*/namespace::*"/>
- -->
&lt;/xxx>
&lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>

<para>applying this to anything (eg itself) gives you
</para>
    <programlisting>&lt;?xml version="1.0" encoding="utf-8" ?>
&lt;xxx xmlns:foo="http://x/y/z" path="foo:that"/></programlisting>

<para>But this relies on some grey areas in the spec about copying namespace
nodes, (which saxon, as Mike pointed out on this list some time back,
interprets slightly differently for copy and copy-of.)
</para>
<para>Mike Kay responds (July 2000)
</para>
<para>Yes I know:-)
But perhaps in version 1+x making the current saxon behaviour for copy
the defined behaviour (for copy and copy-of) would probably be an
improvement.

Still it's a bit of a hack, especially as node-set's an extension.

so an xsl:namespace-declaration that worked like xsl:attribute
would probably be a good addition as well.
    </para>
<para>And David finishes with :-)
</para>

<para>so, a version that only explictly copies element and attribute nodes.
</para>

    <programlisting>
&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                xmlns:saxon="http://icl.com/saxon"
                   extension-element-prefixes="saxon"
                >

&lt;xsl:output method="xml" indent="yes"/>

&lt;xsl:param name="p" select="'foo'"/>
&lt;xsl:param name="n" select="'http://x/y/z'"/>

&lt;xsl:template match="/">
&lt;xsl:variable name="x">
 &lt;xxx path="{$p}:that">
 &lt;xsl:attribute name="{$p}:x" namespace="{$n}"/>
 &lt;/xxx>
&lt;/xsl:variable>
  &lt;xsl:for-each select="saxon:node-set($x)/*">
  &lt;xsl:copy>
   &lt;xsl:copy-of select="@*[local-name()!='x']"/>
  &lt;/xsl:copy>
  &lt;/xsl:for-each>
&lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>


    <programlisting>bash-2.01$ saxon ns3.xsl ns3.xsl
&lt;?xml version="1.0" encoding="utf-8" ?>
&lt;xxx xmlns:foo="http://x/y/z" path="foo:that"/>
</programlisting>



<para>as it doesn't use namespace:: this also works with xt (once you change
the extension namespace)


</para>
   </answer>
  </qandaentry>
</qandaset>
</webpage>
<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
