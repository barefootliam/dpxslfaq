<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-01-24 13:24:07 dpawson"   -->
<!DOCTYPE webpage  SYSTEM "../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="graphs">
<head>
<title>Graphs, directed ...</title>
<summary>graph tools</summary>
  <keywords>docbook faq FAQ XSLT graphs 2</keywords>
</head>
 
<qandaset>


    <qandaentry>
      <question>
	<para>Dealing with graphs</para>
      </question>
      <answer>
	<para role="author">Dimitre Novatchev</para>
	<literallayout>
> I'm looking at graph processing problems as a testbed for this, and
> came across a problem that I haven't been able to solve elegantly. The
> problem is to find "linker" vertexes that a pair of verteces from a
> pre-defined set. For example, if the graph verteces represent cities
> and edges represent flights between them, then given a list of cities,
> find all intermediate cities that you would stop in via a "connecting
> flight".
>
> For example, given the following simple graph:
>
> V1 -> V2 -> V3 -> V4
>         \&lt;- V5 ->/
>
> (V5 points to both V2 and V4), and its XML serialization:
>
>  &lt;graph>
>    &lt;vertex id="V1"/>
>    &lt;vertex id="V2" type="anchor"/>
>    &lt;vertex id="V3"/>
>    &lt;vertex id="V4" type="anchor"/>
>    &lt;vertex id="V5"/>
>    &lt;edge source="V1" target="V2"/>
>    &lt;edge source="V2" target="V3"/>
>    &lt;edge source="V3" target="V4"/>
>    &lt;edge source="V5" target="V2"/>
>    &lt;edge source="V5" target="V4"/>
> &lt;/graph>
>
> I would like to transform this into a second graph where all vertexes
> that "link" two anchor distinct vertexes are flagged as link nodes. In
> this case, there are two anchor vertexes V2 and V4, and I can link
> them through V3 (V2 -> V3 -> V4) and V5 (V2 &lt;- V5 -> V4). Note that
> linked verteces must be distinct, so traversing the V2 &lt;- V1 -> V2
> path should not yield V1 as a link node. So I'd like to see something
> like this:
>
> &lt;graph>
>    &lt;vertex id="V1"/>
>    &lt;vertex id="V2" type="anchor"/>
>    &lt;vertex id="V3" linker="true"/>
>    &lt;vertex id="V4" type="anchor"/>
>    &lt;vertex id="V5" linker="true"/>
>    &lt;edge source="V1" target="V2"/>
>    &lt;edge source="V2" target="V3"/>
>    &lt;edge source="V3" target="V4"/>
>    &lt;edge source="V5" target="V2"/>
>    &lt;edge source="V5" target="V4"/>
> &lt;/graph>
>
> It would be ideal to come up with a generalized solution that would
> let you use 1, 2, .. N intermediate linking nodes. 
</literallayout>

<para>
Here's the general solution and it is quite simple and
straightforward. I use a recursive template based on the following
rule:</para>

	<programlisting>  Paths(/.., X, Z) = Union( {X -> Xi} . Paths(X, Xi, Z) )
</programlisting>
<para>Where the function Paths(Eset, X, Z) returns all paths from X to Z,
that do not include any vertices belonging to the exclusion-set Eset.</para>
<para>
Here {X -> Xi} are all arcs from X.</para>
<para>
Informally,
    {X -> Xi} . Paths(X, Xi, Z)</para>
<para>
is the Cartesian product of the arc {X -> Xi} with the set of paths Paths(X,
Xi, Z), giving a new set of paths.</para>
<para>
As you can see the code of the transformation is 71 lines long, but it
should be noted that for the purpose of readability I write some XPath
expressions on several lines and also almost half of these 71 lines are just
closing tags.</para>
<para>
Here's the code. This transformation:</para>

	<programlisting>&lt;xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:exsl="http://exslt.org/common"
 exclude-result-prefixes="exsl"
 >
  &lt;xsl:output omit-xml-declaration="yes" indent="yes"/>

  &lt;xsl:key name="kNeighbors" match="vertex"
  use="../edge[@target = current()/@id]/@source"/>

  &lt;xsl:key name="kNeighbors" match="vertex"
  use="../edge[@source = current()/@id]/@target"/>

  &lt;xsl:template match="/">
    &lt;xsl:call-template name="getPaths">
      &lt;xsl:with-param name="pNode1"
       select="/*/vertex[@type='anchor'][1]"/>
      &lt;xsl:with-param name="pNode2"
       select="/*/vertex[@type='anchor'][2]"/>
    &lt;/xsl:call-template>
  &lt;/xsl:template>

  &lt;xsl:template name="getPaths">
    &lt;xsl:param name="pNode1" select="/.."/>
    &lt;xsl:param name="pNode2" select="/.."/>
    &lt;xsl:param name="pExcluded" select="/.."/>

    &lt;xsl:for-each select=
           "key('kNeighbors', $pNode1/@id)
                       [not(@id = $pExcluded/@id)]">
      &lt;xsl:choose>
        &lt;xsl:when test="@id = $pNode2/@id">
          &lt;path>
            &lt;xsl:copy-of
             select="/*/edge[$pNode1/@id = @*
                           and
                             $pNode2/@id = @*
                            ]"/>
          &lt;/path>
        &lt;/xsl:when>
        &lt;xsl:otherwise>
          &lt;xsl:variable name="vrtfTail">
            &lt;xsl:call-template name="getPaths">
              &lt;xsl:with-param name="pNode1"
                              select="."/>
              &lt;xsl:with-param name="pNode2"
                              select="$pNode2"/>
              &lt;xsl:with-param name="pExcluded"
                        select="$pExcluded | $pNode1"/>
            &lt;/xsl:call-template>
          &lt;/xsl:variable>

          &lt;xsl:variable name="vTail"
           select="exsl:node-set($vrtfTail)/*"/>

           &lt;xsl:if test="$vTail">
             &lt;path>
               &lt;xsl:copy-of
                  select="/*/edge[$pNode1/@id = @*
                                and
                                  current()/@id = @*
                                  ]"/>

               &lt;xsl:copy-of select="$vTail/*"/>
             &lt;/path>
           &lt;/xsl:if>
        &lt;/xsl:otherwise>
      &lt;/xsl:choose>
    &lt;/xsl:for-each>
  &lt;/xsl:template>
&lt;/xsl:stylesheet>
</programlisting>
<para>when applied on this source.xml:</para>

	<programlisting>&lt;graph>
  &lt;vertex id="V1"/>
  &lt;vertex id="V2" type="anchor"/>
  &lt;vertex id="V3"/>
  &lt;vertex id="V4" type="anchor"/>
  &lt;vertex id="V5"/>
  &lt;edge source="V1" target="V2"/>
  &lt;edge source="V1" target="V3"/>
  &lt;edge source="V2" target="V3"/>
  &lt;edge source="V3" target="V4"/>
  &lt;edge source="V5" target="V2"/>
  &lt;edge source="V5" target="V4"/>
&lt;/graph>
</programlisting>
<para>produces the wanted result:</para>

	<programlisting>&lt;path>
   &lt;edge source="V1" target="V2"/>
   &lt;edge source="V1" target="V3"/>
   &lt;edge source="V3" target="V4"/>
&lt;/path>
&lt;path>
   &lt;edge source="V2" target="V3"/>
   &lt;edge source="V3" target="V4"/>
&lt;/path>
&lt;path>
   &lt;edge source="V5" target="V2"/>
   &lt;edge source="V5" target="V4"/>
&lt;/path>
</programlisting>
<para>
These are all three different paths (with all nodes in the path only
once) from V2 to V4 in the graph described by the xml above.</para>

<para>The first solution:</para>
	<literallayout>
     V2 -> V1 -> V3 ->V4
</literallayout>
<para>is 3 edges long.</para>
<para>
The other two are two edges long each (Note that I added to your original
graph structure a new arc from V1 to V3 in order to make it more "general").</para>
<para>

I hope this helped in this specific problem and also to answer
positively your questions about the expressiveness of XPath/XSLT and
the appropriateness of XSLT as a tool for solving this type of
problems.</para>


<para>
Sjoerd Visscher adds</para>

	<literallayout>> It would be ideal to come up with a generalized solution that would
>  let you use 1, 2, .. N intermediate linking nodes. I've been able to
> get this working with nested loops, but it isn't particularly
>  declarative or speedy, and is certainly more verbose than I'd like,
>  so I'm wondering if anyone here has insights into how to do this
>  elegantly and in XSLT/XPath style. For example, is it possible to
>  write a single XPath expression that will select &lt;vertex>
>  elements that obey the above criteria? If not, does anyone have any
>  suggestions for how to code this effectively and efficiently with
>  XSLT?
</literallayout>
<para>The following XSL transformation does what you want:</para>

	<programlisting>&lt;?xml version="1.0"?>
&lt;xsl:transform version="1.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

&lt;xsl:key name="a" match="/graph/vertex[@type='anchor']" use="@id" />
&lt;xsl:key name="e1" match="/graph/edge/@target" use="../@source" />
&lt;xsl:key name="e2" match="/graph/edge/@source" use="../@target" />

&lt;!-- identity template -->
&lt;xsl:template match="@* | node()">
  &lt;xsl:copy>
    &lt;xsl:apply-templates select="@* | node()" />
  &lt;/xsl:copy>
&lt;/xsl:template>

&lt;xsl:template match="vertex">
  &lt;xsl:copy>
    &lt;xsl:apply-templates select="@* | node()" />
    &lt;!-- more than one edge to an anchor vertex? -->
    &lt;xsl:if test="count(key('a',key('e1',@id)|key('e2',@id)))&amp;gt;1">
      &lt;xsl:attribute name="linker">true&lt;/xsl:attribute>
    &lt;/xsl:if>
  &lt;/xsl:copy>
&lt;/xsl:template>

&lt;/xsl:transform>
</programlisting>
<para>I've used the key function here as both a look-up and as a filter shortcut. The neat thing about the key function is that it returns a set of distinct nodes. F.e. if you'd have two edges from V1 to V2, the expression key('e1', @id) returns 2 nodes, but when put through the key function again to find the anchor vertices, there's only one result: the V2 vertex.</para>

<para>Extending this to a generalized solution is still hard though...</para>


      </answer>
    </qandaentry>






 </qandaset>
</webpage>


