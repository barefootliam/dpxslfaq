<?xml version="1.0" encoding="iso-8859-1" ?>
<!-- Time-stamp: "2004-06-27 16:53:27 dpawson"   -->
<!DOCTYPE webpage  SYSTEM "../../docbook/website/schema/dtd/website.dtd">

<webpage navto="yes" id="sort2">
<head>
<title>Sorting</title>
<summary>Sorting</summary>
  <keywords> faq FAQ XSLT2 Sorting</keywords>
</head>
 
<qandaset>
   

  
 <qandaentry>
      <question>
	<para>Copying with Sorting</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>


<para>XSLT 2.0 allows you to create a sorted sequence of nodes using the
sort() function, with named sort keys:</para>
	<programlisting>
&lt;xsl:sort-key name="sk1">
  &lt;xsl:sort select="exp2"/>
&lt;/xsl:sort-key>

&lt;xsl:copy-of select="sort('sk1', exp)"/>
</programlisting>
      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>Topological sort</para>
      </question>
      <answer>
	<para role="author">Bill Keese  and Dimitre Novatchev</para>




<para>Regarding the post from two years ago about topological sorting 
	  (<ulink
	    url="http://www.biglist.com/lists/xsl-list/archives/200101/msg00161.html">Archive</ulink>),
here is another approach that I came up with.  To me it seems to be more 
in the spirit of XSLT, ie, writing functionally rather than 
procedurally.  Tell me what you think.</para>
<para>
Topological sort refers to printing the nodes in a graph such that you 
print a node before you print any nodes that reference that node.  
Here's an example of a topologically sorted list:</para>
	<programlisting>
        &lt;element id="bar"/>
        &lt;element id="bar2"/>
        &lt;element id="foo">
            &lt;idref  id="bar"/>
        &lt;/element>
</programlisting>
<para>My algorithm is simply:</para>

	<simplelist>
	  <member>   1. each node gets a weight which is greater than the weight of any 
nodes it references
</member>
	  <member>   2.  sort by weight</member>
</simplelist>

<para>The algorithm is O(n^2) for a simple XSLT processor, but it would be 
O(n) if the XSLT processor was smart enough to cache the values returned 
from the computeWeight(node) function.  Does saxon do this?  Maybe it 
would if I used keys.</para>

<para>Here is the code.  Note that it's XSLT V2 (although it could be written 
more verbosely in XSLT V1).</para>

	<programlisting>&lt;xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:bill="http://bill.org"
    version="2.0">
</programlisting>
<para>Here's the code to compute the weight of a node  (This code doesn't 
detect circular dependencies, but it should be easy to add.  That's left 
as an exercise to the reader. :-)</para>
	<programlisting>
    &lt;xsl:function name="bill:computeWeight" as="xs:integer*">
        &lt;xsl:param name="node"/>
        &lt;!-- generate a sequence containing the weights of each node I 
reference -->
        &lt;xsl:variable name="referencedNodeWeights" as="xs:integer*">
            &lt;xsl:sequence select="0"/>
            &lt;xsl:for-each select="$node/idref/@id">
                &lt;xsl:sequence>
                    &lt;xsl:value-of 
select="bill:computeWeight(//element[@id=current()])"/>
                &lt;/xsl:sequence>
            &lt;/xsl:for-each>
        &lt;/xsl:variable>
        &lt;!-- make my weight higher than any of the nodes I reference -->
        &lt;xsl:value-of select="max($referencedNodeWeights)+1"/>
    &lt;/xsl:function>
</programlisting>
<para>Here's the driver code, that sorts the elements according to their weight.</para>
	<programlisting>
    &lt;xsl:template match="/">
        &lt;xsl:for-each select="top/element">
            &lt;xsl:sort select="bill:computeWeight(.)" data-type="number"/>
            &lt;xsl:copy-of select="."/>
        &lt;/xsl:for-each>
    &lt;/xsl:template>
</programlisting>


      </answer>
    </qandaentry>
 




    <qandaentry>
      <question>
	<para>Sorting on names with and without spaces</para>
      </question>
      <answer>
	<para role="author">Michael Kay</para>
	<literallayout>
> I am attempting to sort an a list of personal names. All of the names 
> consist of either a first name followed by a last name or of a last 
> name only (there are no middle names). Both parts of the name, when 
> present, are enclosed within the one tag (span) which has a 
> class='person'
> attribute, the same tag is used to enclose a last name only. I am 
> attempting to sort by last name like so
> 
> &lt;xsl:for-each select="html/body//span[@class='person']">
> &lt;xsl:sort select="substring-after(., ' ')"/> &lt;xsl:sort select="."/> 
> &lt;xsl:sort select="substring-before(., ' ')"/>
> 
> The problem is that names consisting of a last name only appear first 
> in my alphabetical sequence and are sorted; these are followed by 
> names with a first name and a last name and these are also sorted. I 
> require one alphabetical list rather than two.
> 
> Can this be done in one fell swoop, without having to write an XSL 
> style sheet for the file consisting of two alphabetical sequences?
	</literallayout>


<para>As is so often the case, it's easy in 2.0:</para>
<programlisting>
&lt;xsl:sort select="if (contains(., ' ')) 
    then substring-after(., ' ') 
     else string(.)"/></programlisting>


 <para>This xml</para>
<programlisting>
&lt;names>
  &lt;span class="person">Jenofsky&lt;/span>
  &lt;span class="person">Jones&lt;/span>
  &lt;span class="person">Zubbard&lt;/span>
  &lt;span class="person">Bob Madison&lt;/span>
  &lt;span class="person">Oscar Madison&lt;/span>
  &lt;span class="person">Felix Unger&lt;/span> 
&lt;/names> </programlisting>



 <programlisting>&lt;xsl:template match="names">
  &lt;xsl:apply-templates select="span">
           &lt;xsl:sort select="if (contains(., ' ')) then 
                substring-after(., ' ') else string(.)" 
                data-type="text" case-order="upper-first"/>
                      
    &lt;/xsl:apply-templates>
 &lt;/xsl:template></programlisting>

      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Sort  based on numbers and characters.</para>
      </question>
      <answer>
	<para role="author">Andrew Welch</para>
	<para>With this input</para>



<programlisting>&lt;doc>
&lt;n>01&lt;/n>
&lt;n>10&lt;/n>
&lt;n>Alpha&lt;/n>
&lt;n>30&lt;/n>
&lt;n>100&lt;/n>
&lt;n>2&lt;/n>


&lt;/doc>
</programlisting>

	<para>With this styleseet</para>
	<programlisting>
&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;xsl:stylesheet version="2.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   &lt;xsl:strip-space elements="*"/>
   &lt;xsl:output method="xml" indent="yes"/>


   &lt;xsl:template match="/doc">
     &lt;xsl:apply-templates>
       &lt;xsl:sort select="replace(., '\s\D*', '')" data-type="number"/>
       &lt;xsl:sort select="replace(., '\d*\s', '')" data-type="text"/>

     &lt;/xsl:apply-templates>
   &lt;/xsl:template>


   &lt;xsl:template match="n">
     &lt;xsl:message>
       &lt;xsl:value-of select="replace(.,'^ *0+','')"/>
     &lt;/xsl:message>
   &lt;/xsl:template>

&lt;/xsl:stylesheet></programlisting>
      </answer>
    </qandaentry>



    <qandaentry>
      <question>
	<para>Indented output for human readability</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>



<para>If I needed more control in a pure xslt way I'd probably do a 2nd pass
over the result tree either as a 2nd transform or by using a variable
and then a special mode to apply templates, something like</para>

<programlisting>&lt;xsl:template match="*" mode="indent">
&lt;xsl:text>&amp;#10;&lt;/xsl:text>
&lt;xsl:for-each 
  select="ancestor::*">&lt;xsl:text>  &lt;/xsl:text>&lt;/xsl:for-each>
&lt;xsl:copy>
&lt;xsl:copy-of select="@*"/>
&lt;xsl:apply-templates mode="indent"/>
&lt;xsl:text>&amp;#10;&lt;/xsl:text>
&lt;xsl:for-each 
  select="ancestor::*">&lt;xsl:text>  &lt;/xsl:text>&lt;/xsl:for-each>
&lt;xsl:copy>
&lt;/xsl:template></programlisting>
<para>
Then you can ealy add special templates</para>
<programlisting>
&lt;xsl:template match="foo" mode="indent">
&lt;xsl:text>&amp;#10;&amp;#10;&lt;/xsl:text>
&lt;xsl:comment>double spaced&lt;/xsl:comment>
&lt;xsl:for-each select="ancestor::*">&lt;xsl:text>  &lt;/xsl:text>&lt;/xsl:for-each>
&lt;xsl:copy>
&lt;xsl:copy-of select="@*"/>
&lt;xsl:apply-templates mode="indent"/>
&lt;xsl:text>&amp;#10;&lt;/xsl:text>
&lt;xsl:for-each select="ancestor::*">&lt;xsl:text>  &lt;/xsl:text>&lt;/xsl:for-each>
&lt;xsl:copy>
&lt;/xsl:template></programlisting>
<para>
This way the indenting doesn't intefere with your main transform and can
easily be put in a common stylesheet that you include when needed.</para>


      </answer>
    </qandaentry>


    <qandaentry>
      <question>
	<para>Month lookup</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>



<para>I have for quite a few years used this primitive way of translating
from a month number to a month name.</para>
<programlisting>
&lt;xslt:variable name="$months">
 &lt;m>January&lt;/m>&lt;m>February&lt;/m>&lt;m>March&lt;/m>&lt;m>April&lt;/m>
 &lt;m>May&lt;/m>&lt;m>June&lt;/m>&lt;m>July&lt;/m>&lt;m>August&lt;/m>
 &lt;m>September&lt;/m>&lt;m>October&lt;/m>&lt;m>November&lt;/m>&lt;m>December&lt;/m>
&lt;/xsl:variable></programlisting>
<para>
The actual lookup was done using a this expression</para>

<programlisting>&lt;xsl:value-of
          select="$months/m[number($month)]"/></programlisting>
<para>
Where $month can be the string 1 .. 12.</para>
<programlisting>
In XSLT 2.0 you can use</programlisting>
<para>

In xslt2 you could also (more efficiently) do</para>
<programlisting>
&lt;xslt:variable name="months" as="item()*" select=
"('January','February','March','April','May','June','July','August',
'September','October','November','December')"/></programlisting>
<programlisting>
&lt;xsl:value-of select="$months[xs:integer($month)]"/></programlisting>

      </answer>
    </qandaentry>

    <qandaentry>
      <question>
	<para>Sorting, with variables</para>
      </question>
      <answer>
	<para role="author">Abel Braaksma</para>
	<literallayout>

My requirement is that I have an unknown number of sort conditions
that I want to apply to a for-each, for-each-group, or apply-templates
construct.

Is there a way to dynamically define the number of &lt;xsl:sort>
statements to use based on selecting parameters from an xml?

For example, &lt;list> might have 0 to n &lt;sort/> nodes:

&lt;list>
 &lt;address>
   &lt;name/>
   &lt;street/>
   &lt;city/>
 &lt;/address>
 &lt;!-- more address nodes -->

 &lt;sort order="1">name&lt;/sort>
 &lt;sort order="2">city&lt;/sort>
&lt;!-- might have more sort nodes -->
&lt;/list>

If I have no nodes, I would want to omit the &lt;xsl:sort> statement, etc...
</literallayout>
<para>
Answer</para>
<para>
The easy part is the number of sort statements. Just limit the amount
to, say, 10, and create 10 sort statements. Then:</para>
<para>
1. The 'order' and 'data-type' are AVT, so you can easily get them from
any variable or data structure. Likely something like:
$sortkeys/sort[1]/order etc. This will give you fine granularity control
over descending/ascending, numeric/string. Keep in mind that it is an
error when the order or data-type evaluates to empty, so make sure to
use defaults in that case.</para>
<para>
2. The select statements are a bit tricky. You can leave them simple if
you only need to sort on one node with a given name (from your sortkey)
and use local-name() or name() functions to match for it. You can
evaluate an xpath (not sure saxon:evaluate is the way to go) or you can
decide to create a simple function that takes the current node and
resolves your (simplified) xpath if it is more then just a node name.</para>
<para>
3. The order of the sortkeys is something you can resolve in several
XSLT native ways. That shouldn't be that hard.</para>
<para>
Combined, this looks something like this:</para>
<programlisting>
&lt;!-- order of these elements is the order for the sort-key -->
&lt;xsl:variable name="sortkey">
  &lt;key nodename='name' order='ascending' type='string' />
  &lt;key nodename='street' order='ascending' type='string' />
  &lt;key nodename='birth-year' order='ascending' type='numberic' />
&lt;/xsl:variable>

&lt;xsl:apply-templates select="somenode">
   &lt;xsl:sort select="*[local-name() = $sortkey/key[1]/@nodename"
       order="{$sortkey/key[1]/@order}"
data-type="{$sortkey/key[1]/@type}"/>
   &lt;xsl:sort select="*[local-name() = $sortkey/key[1]/@nodename"
       order="{$sortkey/key[2]/@order}"
data-type="{$sortkey/key[2]/@type}"/>
   &lt;xsl:sort select="*[local-name() = $sortkey/key[1]/@nodename"
       order="{$sortkey/key[3]/@order}"
data-type="{$sortkey/key[3]/@type}"/>
.... etc (10x) ...
&lt;/xsl:apply-templates>
</programlisting>
<para>
You can generalize this to some further extend, of course.</para>
<para>
If you want even more control, you should consider making a two-pass
transformation where in the first pass you create the XSLT that contains
the correct sortkeys. FXSL has many examples of how to do multi-pass on
XSLT alone.</para>

<para>
Andrew Welch offers</para>
<para>

Here's a way of sorting dynamically without extensions:</para>

	<programlisting>&lt;?xml version="1.0"?>
&lt;xsl:stylesheet version="2.0"
xmlns:xs="http://www.w3.org/2001/XMLSchema";
xmlns:xsl="http://www.w3.org/1999/XSL/Transform";
xmlns:fn="fn">

&lt;xsl:variable name="employees" select="/employees/employee"/>
&lt;xsl:variable name="sortValues" select="/employees/sort" as="xs:string+"/>

&lt;xsl:function name="fn:performSort" as="element()+">
   &lt;xsl:param name="list" as="element()+"/>
   &lt;xsl:param name="sortKey" as="xs:string+"/>

   &lt;xsl:variable name="sortedList" as="element()+">
       &lt;xsl:for-each select="$list">
           &lt;xsl:sort select="*[name() = $sortKey[1]]"/>
           &lt;xsl:copy-of select="."/>
       &lt;/xsl:for-each>
   &lt;/xsl:variable>

   &lt;xsl:sequence select="if ($sortKey[2]) then
                           fn:performSort($sortedList,
subsequence($sortKey, 2))
                           else $sortedList"/>
&lt;/xsl:function>

&lt;xsl:template match="/">
   &lt;xsl:sequence select="fn:performSort($employees, $sortValues)"/>
&lt;/xsl:template>

&lt;/xsl:stylesheet>
</programlisting>
<para>You call the performSort() function with a list of elements to sort,
and a list of child element names to sort with, and it recursively
sorts by each one.</para>
	
      </answer>
    </qandaentry>

   <qandaentry>
      <question>
	<para>Sort a tokenized list</para>
      </question>
      <answer>
	<para role="author">David Carlisle</para>
	<literallayout>


I've been attempting to write a template which sorts a tokenized list of
pairs of numbers with little success.

Here's what I've been attempting to do:

Given a string like:
"1 0 2 1 1 2 1 3 1 4 2 0 1 1 2 3 2 4 6 0 5 0 10 0 10 1 10 2"

I want to tokenize and build pairs like:

"1 0", "2 1", "1 2", "1 3", "1 4", "2 0", "1 1", "2 3", "2 4", "6 0", "5
0", "10 0", "10 1", "10 2"
</literallayout>

<para>answer</para>
	<literallayout>
xsl:stylesheet version="2.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

%lt;xsl:param name="s" select="'1 0 2 1 1 2 1 3 1 4 2 0 1 1 2 3 2 4 6 0 5 0 10 0 10 1 10 2'"/>

%lt;xsl:template name="main">
%lt;xsl:for-each-group select="tokenize($s,'\s+')" group-by="(position()-1) idiv 2">
  %lt;xsl:sort select="number(current-group()[1])"/>
  %lt;xsl:sort select="number(current-group()[2])"/>
: %lt;xsl:value-of select="current-group()"/>
%lt;/xsl:for-each-group>
%lt;/xsl:template>

%lt;/xsl:stylesheet>
</literallayout>

	<literallayout>$ saxon9 -it main sort.xsl
%lt;?xml version="1.0" encoding="UTF-8"?>
: 1 0
: 1 1
: 1 2
: 1 3
: 1 4
: 2 0
: 2 1
: 2 3
: 2 4
: 5 0
: 6 0
: 10 0
: 10 1
: 10 2</literallayout>




      </answer>
    </qandaentry>




 </qandaset>
</webpage>


<!--
     Local Variables:
     mode: xml
     sgml-parent-document: ("dpawson.xml" "website" )
     End:
   -->
